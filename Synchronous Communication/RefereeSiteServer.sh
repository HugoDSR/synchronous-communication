echo "Compressing data to be sent to the RefereeSite Server node."
rm -rf refereeSiteServer.zip
zip -rq refereeSiteServer.zip RefereeSiteServer Comms
echo "Transfering data to the RefereeSite Server node."
sshpass -f password ssh cd0301@l040101-ws09.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws09.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp refereeSiteServer.zip cd0301@l040101-ws09.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the RefereeSite Server node."
sshpass -f password ssh cd0301@l040101-ws09.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q refereeSiteServer.zip'
echo "Compiling program files at the RefereeSite Server node."
sshpass -f password ssh cd0301@l040101-ws09.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the RefereeSite Server node."
sshpass -f password ssh cd0301@l040101-ws09.ua.pt 'cd teste/TheRopeGameVDist ; java RefereeSiteServer.RefereeSiteMain'
