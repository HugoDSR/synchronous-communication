/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContestantClient;

import Comms.Parameters;

/**
*
* @author Hugo Rocha nmec 68511
* @author Miguel Antunes nmec 68484
*/
public class ContestantMain {

	public static void main(String[] args) {
		BenchStub bench = new BenchStub(Parameters.gethostNameBenchServer(), Parameters.getportBenchServer());
		PlaygroundStub playground = new PlaygroundStub(Parameters.gethostNamePlaygroundServer(),
				Parameters.getportPlaygroundServer());

		/* Thread Creation */
		Contestant[][] contestant = new Contestant[Parameters.MAX_TEAMS][Parameters.MAX_CONTESTANTS];

		for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
			for (int ctId = 0; ctId < Parameters.MAX_CONTESTANTS; ctId++) {
				contestant[team][ctId] = new Contestant(ctId, team, bench, playground);
			}
		}

		/* Simulation kickoff */
		for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
			for (int ctId = 0; ctId < Parameters.MAX_CONTESTANTS; ctId++) {
				contestant[team][ctId].start();
			}
		}

		/* Waits simulation to end */
		try {
			for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
				for (int ctId = 0; ctId < Parameters.MAX_CONTESTANTS; ctId++) {
					contestant[team][ctId].join();
				}
			}
		} catch (InterruptedException e) {
		}
		
		bench.shutdown();
		playground.shutdown();

	}

}
