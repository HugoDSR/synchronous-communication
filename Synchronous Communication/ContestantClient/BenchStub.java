/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContestantClient;

import Comms.*;
import genclass.GenericIO;

/**
 *
 * @author hugo
 */
public class BenchStub {

	/**
	 * Name of computational sister where server is located
	 *
	 * @SerialField serverHostName
	 */
	private String serverHostName;

	/**
	 * Number of server listening port
	 *
	 * @SerialField serverPortNumber
	 */
	private int serverPortNumber;

	/**
	 * PlaygroundStub Instantiation
	 *
	 * @param hostname
	 *            name of the system where server is located
	 * @param port
	 *            number of server listening port
	 *
	 */
	public BenchStub(String hostname, int port) {
		serverHostName = hostname;
		serverPortNumber = port;
	}

	public boolean seatDown(int teamId, int cntId, int strength) {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.CNTSEAT, teamId, cntId, strength);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);
		if ((inMessage.getMsgType() != Message.SEATEDCONT)) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
		if (inMessage.hasgameEnded()) {
			return true; // operação bem sucedida
		} else {
			Contestant ct = (Contestant) Thread.currentThread();
			ct.setSelected(false);
			return false; // operação falhou
		}
	}

	public void followCoachAdvice(int teamId, int cntId) {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.CNTFOLLOW, teamId, cntId);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);
		if ((inMessage.getMsgType() != Message.FOLLOWEDADV)) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}

		if (inMessage.getMsgType() == Message.FOLLOWEDADV) {
			Contestant ct = (Contestant) Thread.currentThread();
			ct.setSelected(true);
			ct.setPlaygroundId(inMessage.getcontestantPlaygroundId());
			ct.setStrength(inMessage.getContestantStrength());
		}
		
		con.close();
	}
	
	public void shutdown() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
        Message inMessage, outMessage;
		String inString, outString;
        while (!con.open()) // aguarda ligação
        {
                 try {
                          Thread.currentThread().sleep((long) (500));
                 } catch (InterruptedException e) {
                 }
        }
        outMessage = new Message(Message.SHUT); 
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);
        if ((inMessage.getMsgType() != Message.ACK)) {
                 GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
                 GenericIO.writelnString(inMessage.toString());
                 System.exit(1);
        }
        con.close();
	}

}
