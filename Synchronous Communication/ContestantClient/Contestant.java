/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContestantClient;

import Comms.*;
import java.util.Random;

/**
 *
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class Contestant extends Thread {

         /**
          * Contestant Id
          *
          * @serialField contestantId
          */
         private int contestantId;

         /**
          * Contestant Team
          *
          * @serialField contestantTeam
          */
         private int contestantTeam;

         /**
          * Contestant Strength
          *
          * @serialField strength
          */
         private int strength;
         
         /**
          * Contestant Id on playground
          * 
          * @serialField playgroundId
          */
         private int playgroundId;
         
         /**
          * Contestant is selected to play
          * 
          * @serialField selected
          */
         private boolean selected;

         /**
          * Contestant State
          *
          * @serialField contestantState
          */
         private ContestantState contestantState;

         /**
          * Stub of shared regions
          *
          * @serialField bench
          * @serialField playground
          */
         private BenchStub benchStub;
         private PlaygroundStub playgroundStub;

         /**
          * Instantiation of a Contestant
          *
          * @param refereeId referee identification
          * @param strength contestant strength
          * @param benchStub stub to bench
          * @param playgroundStub stub to playground
          *
          */
         public Contestant(int contestantId, int contestantTeam,  BenchStub benchStub, PlaygroundStub playgroundStub) {
                  super("Contestant_"+contestantTeam+"_" + contestantId);

                  this.contestantId = contestantId;
                  this.contestantTeam = contestantTeam;
                  Random rd = new Random();
                  strength = rd.nextInt(20) + 1;
                  contestantState = ContestantState.SLEEP;
                  this.benchStub = benchStub;
                  this.playgroundStub = playgroundStub;
                  selected = false;
                  playgroundId=0;
         }

         /**
          * Life cycle of referee thread
          */
         @Override
         public void run() {
        	 System.out.println("O Contestant "+contestantId+" da equipa "+contestantTeam+" começou");
                  while (!benchStub.seatDown(contestantTeam, contestantId, strength)) {
                           benchStub.followCoachAdvice(contestantTeam, contestantId);
                           playgroundStub.followCoachAdvise(contestantTeam, contestantId, strength, playgroundId);
                           playgroundStub.getReady(contestantTeam, contestantId);
                           pullTheRope();
                           playgroundStub.amDone(contestantTeam, playgroundId);
                  }
                  System.out.println("O Contestant "+contestantId+" da equipa "+contestantTeam+" terminou");
         }

         private void pullTheRope() {
                  try {
                           Thread.sleep((long) (Math.random() * 40 + 1));
                  } catch (InterruptedException err) {
                  }
         }
         
         public void setPlaygroundId(int playgroundId){
        	 this.playgroundId=playgroundId;
         }
         
         public void setSelected(boolean flag){
        	 selected=flag;
         }
         
         public void setStrength(int strength){
        	 this.strength=strength;
         }

}
