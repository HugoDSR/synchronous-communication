echo "Compressing data to be sent to the Coach Client node."
rm -rf coachClient.zip
zip -rq coachClient.zip CoachClient Comms
echo "Transfering data to the Coach Client node."
sshpass -f password ssh cd0301@l040101-ws05.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws05.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp coachClient.zip cd0301@l040101-ws05.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the Coach Client node."
sshpass -f password ssh cd0301@l040101-ws05.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q coachClient.zip'
echo "Compiling program files at the Coach Client node."
sshpass -f password ssh cd0301@l040101-ws05.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the Coach Client node."
sshpass -f password ssh cd0301@l040101-ws05.ua.pt 'cd teste/TheRopeGameVDist ; java CoachClient.CoachMain'
