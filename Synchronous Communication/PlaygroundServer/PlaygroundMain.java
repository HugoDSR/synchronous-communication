/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PlaygroundServer;

import java.net.SocketTimeoutException;

import Comms.Parameters;
import Comms.Server_Com;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class PlaygroundMain {

	private static final int portNumb = Parameters.getportPlaygroundServer();
	public static boolean waitConnection = true;

	public static void main(String[] args) {
		Playground playground;
		PlaygroundInterface playgroundInterface;

		Server_Com scon, sconi;
		PlaygroundAps aps;

		scon = new Server_Com(portNumb);
		scon.start();

		LoggerStub logger = new LoggerStub(Parameters.gethostNameLoggerServer(), Parameters.getportLoggerServer());

		playground = new Playground(logger);
		playgroundInterface = new PlaygroundInterface(playground);

		System.out.println("O serviço foi establecido");
		System.out.println("O servidor esta em escuta");

		while (waitConnection) {
			try {
				sconi = scon.accept();
				aps = new PlaygroundAps(sconi, playgroundInterface);
				aps.start();
			} catch (SocketTimeoutException e) {
			}
		}
		
		logger.shutdown();
		scon.end();
		System.out.println("O servidor foi desactivado.");
	}
}
