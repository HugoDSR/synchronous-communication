package PlaygroundServer;

import Comms.Semaphore;
import Comms.ContestantState;
import Comms.CoachState;
import Comms.RefereeState;


import Comms.Parameters;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class Playground {

	/* Object Criation */

	Semaphore mutex;
	Semaphore[][] contestantsOnPlayground;
	Semaphore[] coachesOnPlayground;
	Semaphore refereeOnPlayground;
	LoggerStub logger;
	private PlaygroundAps[][] players;
	int[][] posRope;
	int[] ctOnRope;
	int[] teamStrenght;
	int[] scoreBoard;
	public int matchesPlayed;
	int playerNum;
	int coachArrived;

	public Playground(LoggerStub logger) {
		mutex = new Semaphore();
		mutex.up();
		this.logger=logger;
		contestantsOnPlayground = new Semaphore[2][3];
		coachesOnPlayground = new Semaphore[2];
		refereeOnPlayground = new Semaphore();
		players = new PlaygroundAps[2][3];
		posRope = new int[2][3];
		ctOnRope = new int[2];
		teamStrenght = new int[2];
		scoreBoard = new int[2];
		matchesPlayed = 0;
		playerNum = 0;
		coachArrived = 0;

		for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
			coachesOnPlayground[team] = new Semaphore();
			for (int ct = 0; ct < Parameters.MAX_CT_ON_ROPE; ct++) {
				contestantsOnPlayground[team][ct] = new Semaphore();
			}
		}
	}

	/* Funções dos Contestants */

	public void followCoachAdvise(int contestantTeam, int contestantId) {
		PlaygroundAps ct = (PlaygroundAps) Thread.currentThread();
		contestantsOnPlayground[contestantTeam][ct.getPlaygroundId()].down();
		mutex.down();
		logger.setctState(ContestantState.STAND_IN_POSITION, contestantTeam, contestantId);
		players[contestantTeam][ct.getPlaygroundId()] = ct;
		posRope[contestantTeam][ctOnRope[contestantTeam]] = contestantId;
		logger.writePosition(posRope, contestantTeam, ct.getPlaygroundId());
		ctOnRope[contestantTeam]++;
		mutex.up();
	}

	public void getReady(int contestantTeam, int contestantId) {
		mutex.down();
		playerNum++;
		logger.setctState(ContestantState.DO_YOUR_BEST, contestantTeam, contestantId);
		if (playerNum == 6) {
			playerNum = 0;
			coachesOnPlayground[0].up();
			coachesOnPlayground[1].up();
		}
		mutex.up();
	}

	public void amDone(int contestantTeam, int contestantId) {
		contestantsOnPlayground[contestantTeam][contestantId].down();
		mutex.down();
		playerNum++;
		if (playerNum == 6) {
			refereeOnPlayground.up();
			playerNum = 0;
		}
		mutex.up();
		contestantsOnPlayground[contestantTeam][contestantId].down();
	}

	/* Funções dos Coaches */

	public void callContestants(int coachTeam) {
		coachesOnPlayground[coachTeam].down();
		mutex.down();
		logger.setcState(CoachState.ASSEMBLE_TEAM, coachTeam);
		for (int ct = 0; ct < 3; ct++) {
			contestantsOnPlayground[coachTeam][ct].up();
		}
		mutex.up();
	}

	public void informReferee(int coachTeam) {
		coachesOnPlayground[coachTeam].down();
		mutex.down();
		logger.setcState(CoachState.WATCH_TRIAL, coachTeam);
		coachArrived++;
		if (coachArrived == 2) {
			refereeOnPlayground.up();
			coachArrived = 0;
		}
		mutex.up();
		coachesOnPlayground[coachTeam].down();
	}

	/* Funções do Referee */

	public void startTrial() {
		refereeOnPlayground.down();
		mutex.down();
		logger.setrState(RefereeState.WAIT_FOR_TRIAL_CONCLUSION);
		for (int ct = 0; ct < 3; ct++) {
			contestantsOnPlayground[0][ct].up();
			contestantsOnPlayground[1][ct].up();
		}
		mutex.up();
	}

	public void assertTrialDecision() {
		refereeOnPlayground.down();
		mutex.down();
		for (int team = 0; team < 2; team++) {
			for (int ct = 0; ct < 3; ct++) {
				teamStrenght[team] += players[team][ct].getStrength();
			}
		}
		if (teamStrenght[0] > teamStrenght[1]) {
			scoreBoard[0]++;
		}
		if (teamStrenght[1] > teamStrenght[0]) {
			scoreBoard[1]++;
		}
		ctOnRope[0] = 0;
		ctOnRope[1] = 0;
		for (int team = 0; team < 2; team++) {
			coachesOnPlayground[team].up();
			for (int ct = 0; ct < 3; ct++) {
				contestantsOnPlayground[team][ct].up();
			}
		}
		mutex.up();
	}
	
	public void wakeUpCoachesPlayground() {
		for (int team = 0; team < 2; team++) {
			coachesOnPlayground[team].up();
		}
	}

	public int[] getScoreBoard() {
		return scoreBoard;
	}

	public int getMatchesNum() {
		return matchesPlayed;
	}

	public void clearscoreBoard() {
		scoreBoard[0] = 0;
		scoreBoard[1] = 0;
	}

}
