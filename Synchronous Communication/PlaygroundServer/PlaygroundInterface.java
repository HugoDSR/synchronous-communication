/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PlaygroundServer;

import Comms.*;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class PlaygroundInterface {

	/**
	 * Playground (represents the service to be done)
	 *
	 * @serialField playground
	 */
	private Playground playground;
	
	private volatile int shut;

	/**
	 * Instantiation of playground interface
	 *
	 * @param playground
	 */
	public PlaygroundInterface(Playground playground) {
		this.playground = playground;
	}

	public Message processAndReply(Message inMessage) throws MessageException {

		PlaygroundAps proxy;
		Message outMessage = null;

		/* message received validation */
		switch (inMessage.getMsgType()) {
		case Message.CNTFOLLOW:
			if (inMessage.getContestantTeam() == -1 || inMessage.getContestantId() == -1) {
				throw new MessageException(
						"Dados de entrada de Contestant para o metodo followCoachAdvice() invalidos!", inMessage);
			}
			break;
		case Message.CNTGETREADY:
			if (inMessage.getContestantTeam() == -1 || inMessage.getContestantId() == -1) {
				throw new MessageException("Dados de entrada de Contestant para o metodo getReady() invalidos!",
						inMessage);
			}
			break;
		case Message.CNTDONE:
			if (inMessage.getContestantTeam() == -1 || inMessage.getContestantId() == -1) {
				throw new MessageException("Dados de entrada de Contestant para o metodo amDone() invalidos!",
						inMessage);
			}
			break;
		case Message.COACALLSCONT:
			if (inMessage.getCoachTeam() == -1) {
				throw new MessageException("Dados de entrada de Coach para o metodo CallContestant() invalidos!",
						inMessage);
			}
			break;
		case Message.COACHINFREF:
			if (inMessage.getCoachTeam() == -1) {
				throw new MessageException("Dados de entrada de Coach para o metodo InformReferee() invalidos!",
						inMessage);
			}
			break;
		}

		/* message processed */
		switch (inMessage.getMsgType()) {
		case Message.CNTFOLLOW:
			proxy = (PlaygroundAps) Thread.currentThread();
			proxy.setContestantId(inMessage.getContestantId());
			proxy.setContestantTeam(inMessage.getContestantTeam());
			proxy.setStrength(inMessage.getContestantStrength());
			proxy.setPlaygroundId(inMessage.getcontestantPlaygroundId());
			playground.followCoachAdvise(inMessage.getContestantTeam(), inMessage.getContestantId());

			outMessage = new Message(Message.FOLLOWEDADV);
			break;

		case Message.CNTGETREADY:
			playground.getReady(inMessage.getContestantTeam(), inMessage.getContestantId());
			outMessage = new Message(Message.CNTISREADY);
			break;

		case Message.CNTDONE:
			proxy = (PlaygroundAps) Thread.currentThread();
			proxy.setPlaygroundId(inMessage.getcontestantPlaygroundId());
			playground.amDone(inMessage.getContestantTeam(), inMessage.getContestantId());
			outMessage = new Message(Message.CNTDONEACKN);
			break;

		case Message.COACALLSCONT:
			playground.callContestants(inMessage.getCoachTeam());
			outMessage = new Message(Message.CONTCALLED);
			break;

		case Message.COACHINFREF:
			playground.informReferee(inMessage.getCoachTeam());
			outMessage = new Message(Message.COACHINFORMEDREF);
			break;

		case Message.STARTTRIAL:
			playground.startTrial();
			outMessage = new Message(Message.TRIALSTARTED);
			break;

		case Message.ASSERTTRIAL:
			playground.assertTrialDecision();
			outMessage = new Message(Message.TRIALASSERTED);
			break;

		case Message.GETSCOREBOARD:
			int[] scoreBoard = playground.getScoreBoard();
			outMessage = new Message(Message.SCOREBOARDDELIV, scoreBoard);
			break;

		case Message.GETGAMENUM:
			double matchPlayed = (double) playground.getMatchesNum();
			outMessage = new Message(Message.GAMENUMDELIV, matchPlayed);
			break;

		case Message.REFWAKEUPCOAHESP:
			playground.wakeUpCoachesPlayground();
			outMessage = new Message(Message.COAHESWOKENUPP);
			break;

		case Message.REFCLEARSCORE:
			playground.clearscoreBoard();
			outMessage = new Message(Message.SCORECLEAR);
			break;
		case Message.SHUT: // shutdown do servidor
			shut++;
			if (shut == Parameters.SHUTDOWN) {
				PlaygroundMain.waitConnection = false;
				(((PlaygroundAps) (Thread.currentThread())).getScon()).setTimeout(10);
			}
			outMessage = new Message(Message.ACK); // gerar confirmação
			break;
		}

		return outMessage;
	}

}
