package PlaygroundServer;

import Comms.ContestantState;
import Comms.CoachState;
import Comms.RefereeState;
import Comms.Client_Com;
import Comms.Message;

import genclass.GenericIO;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */

public class LoggerStub {

	/**
	 * Name of computational system where server is located
	 *
	 * @SerialField serverHostName
	 */
	private String serverHostName;

	/**
	 * Number of server listening port
	 *
	 * @SerialField serverPortNumber
	 */
	private int serverPortNumber;

	/**
	 * PlaygroundStub Instantiation
	 *
	 * @param hostname
	 *            name of the system where server is located
	 * @param port
	 *            number of server listening port
	 *
	 */
	public LoggerStub(String hostname, int port) {
		serverHostName = hostname;
		serverPortNumber = port;
	}

	public void setctState(ContestantState ctState, int teamId, int id) {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.SETCNTSTATE, teamId, id, ctState);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);

		if (inMessage.getMsgType() != Message.CNTSTATESET) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
	}

	public void setcState(CoachState cState, int team) {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.SETCOASTATE, team, cState);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);

		if (inMessage.getMsgType() != Message.COASTATESET) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
	}

	public void setrState(RefereeState rState) {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.SETREFSTATE, rState);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);

		if (inMessage.getMsgType() != Message.REFSTATESET) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
	}

	public void writePosition(int[][] posRope, int contestantTeam, int playgroundId) {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.UPDATEPOSLOGGER, posRope, contestantTeam, playgroundId);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage= new Message(inString);

		if (inMessage.getMsgType() != Message.POSUPDATEDLOGGER) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
		
	}
	
	public void shutdown() {
			Client_Com con = new Client_Com(serverHostName, serverPortNumber);
         Message inMessage, outMessage;
 		String inString, outString;
         while (!con.open()) // aguarda ligação
         {
                  try {
                           Thread.currentThread().sleep((long) (500));
                  } catch (InterruptedException e) {
                  }
         }
         outMessage = new Message(Message.SHUT);
 		outString = outMessage.toXMLString();
 		con.writeObject(outString);
 		inString = (String) con.readObject();
 		inMessage= new Message(inString);
         if ((inMessage.getMsgType() != Message.ACK)) {
                  GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
                  GenericIO.writelnString(inMessage.toString());
                  System.exit(1);
         }
         con.close();
		}
}
