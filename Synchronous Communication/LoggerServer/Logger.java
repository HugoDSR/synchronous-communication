/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoggerServer;

import Comms.Semaphore;
import genclass.GenericIO;
import genclass.TextFile;
import Comms.Parameters;
import Comms.CoachState;
import Comms.ContestantState;
import Comms.RefereeState;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */

public class Logger {

	private Semaphore mutex;
	private String fileName;
	private String[][] contestantLogState = new String[Parameters.MAX_TEAMS][Parameters.MAX_CONTESTANTS];
	private int[][] contestantLogStrg = new int[Parameters.MAX_TEAMS][Parameters.MAX_CONTESTANTS];
	private String[][] ctPosOnRope = new String[Parameters.MAX_TEAMS][Parameters.MAX_CT_ON_ROPE];
	private String[] coachLogState = new String[Parameters.MAX_COACHES];
	private String refereeLogState;
	private String trialNumber;
	private String centerOftheRope;
	private int gameNumber;
	private boolean matchFinished;
	private int[][] positionOnRope;

	public Logger() {
		mutex = new Semaphore();
		mutex.up();

		fileName = "logging.txt";
		trialNumber = "--";
		centerOftheRope = "--";
		gameNumber = -1;
		matchFinished = false;
		positionOnRope = new int[Parameters.MAX_TEAMS][Parameters.MAX_CT_ON_ROPE];
		refereeLogState = "SMA";
		for (int i = 0; i < Parameters.MAX_TEAMS; i++) {
			coachLogState[i] = "SLEP";
			for (int j = 0; j < Parameters.MAX_CONTESTANTS; j++) {
				contestantLogState[i][j] = "SLP";
			}
			for (int j = 0; j < Parameters.MAX_CT_ON_ROPE; j++) {
				ctPosOnRope[i][j] = "-";
			}
		}

		TextFile log = new TextFile();
		if (!log.openForWriting(".", fileName)) {
			GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " no CONSTRUTOR falhou!");
			System.exit(1);
		}
		log.writelnString("                     Game of the Rope - Description of the internal state");
		if (!log.close()) {
			GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " no CONSTRUTOR falhou!");
			System.exit(1);
		}
		header();
		reportStatus();
		writeNewGame(0, 0);
	}

	private void header() {
		TextFile log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " falhou!");
			System.exit(1);
		}
		log.writelnString(
				"Ref Coa 1 Cont 1 Cont 2 Cont 3 Cont 4 Cont 5 Coa 2 Cont 1 Cont 2 Cont 3 Cont 4 Cont 5       Trial");
		log.writelnString(
				"Sta Stat Sta SG Sta SG Sta SG Sta SG Sta SG Stat Sta SG Sta SG Sta SG Sta SG Sta SG 3 2 1 . 1 2 3 NB PS");
		if (!log.close()) {
			GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " no HEADER falhou!");
			System.exit(1);
		}
	}

	private void reportStatus() {
		TextFile log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " no REPORTSTATUS falhou!");
			System.exit(1);
		}

		String lineStatus = "";
		lineStatus = String.format(
				"%3s %4s %3s %2d %3s %2d %3s %2d %3s %2d %3s %2d %4s %3s %2d %3s %2d %3s %2d %3s %2d %3s %2d %1s %1s %1s %1s %1s %1s %1s %2s %2s",
				refereeLogState, coachLogState[0], contestantLogState[0][0], contestantLogStrg[0][0],
				contestantLogState[0][1], contestantLogStrg[0][1], contestantLogState[0][2], contestantLogStrg[0][2],
				contestantLogState[0][3], contestantLogStrg[0][3], contestantLogState[0][4], contestantLogStrg[0][4],
				coachLogState[1], contestantLogState[1][0], contestantLogStrg[1][0], contestantLogState[1][1],
				contestantLogStrg[1][1], contestantLogState[1][2], contestantLogStrg[1][2], contestantLogState[1][3],
				contestantLogStrg[1][3], contestantLogState[1][4], contestantLogStrg[1][4], ctPosOnRope[0][0],
				ctPosOnRope[0][1], ctPosOnRope[0][2], ".", ctPosOnRope[1][0], ctPosOnRope[1][1], ctPosOnRope[1][2],
				trialNumber, centerOftheRope);
		log.writelnString(lineStatus);
		if (!log.close()) {
			GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " no REPORTSTATUS falhou!");
			System.exit(1);
		}
	}

	public void setctState(ContestantState contestantState, int teamId, int cntId) {
		System.out.println("CONTESTANT_" + teamId + "_" + cntId + "_STATE: " + getState(contestantState));
		if (contestantState == ContestantState.SEAT_AT_BENCH && trialNumber != "--") {
			ctPosOnRope[teamId][getPlaygroundId(cntId, teamId)] = "-";
		}
		contestantLogState[teamId][cntId] = getState(contestantState);
		reportStatus();
	}

	public void setcState(CoachState coachState, int team) {
		System.out.println("COACH_" + team + "_STATE: " + getState(coachState));
		coachLogState[team] = getState(coachState);
		reportStatus();
	}

	public void setrState(RefereeState refState) {
		System.out.println("REF_STATE: " + getState(refState));
		refereeLogState = getState(refState);
		reportStatus();
	}

	public void writeGameWinner(int[] gameScoreBoard) {
		TextFile log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " no WRITEGAMEWINNER falhou!");
			System.exit(1);
		}
		if (gameScoreBoard[0] == Parameters.KNOCKOUT) {
			log.writelnString("Game " + gameNumber + " was won by team 1 by knock out in " + trialNumber + " trials.");
		} else if (gameScoreBoard[1] == Parameters.KNOCKOUT) {
			log.writelnString("Game " + gameNumber + " was won by team 0 by knock out in " + trialNumber + " trials.");
		} else if (gameScoreBoard[0] > gameScoreBoard[1]) {
			log.writelnString("Game " + gameNumber + " was won by team 1 by points.");
		} else if (gameScoreBoard[0] < gameScoreBoard[1]) {
			log.writelnString("Game " + gameNumber + " was won by team 0 by points.");
		} else if (gameScoreBoard[0] == gameScoreBoard[1]) {
			log.writelnString("Game " + gameNumber + " was a draw.");
		}
		if (!log.close()) {
			GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " no WRITEGAMEWINNER falhou!");
			System.exit(1);
		}
		if (gameNumber == 2) {
			matchFinished = true;
		}
	}

	public void writeNewGame(int gamesplayed, int trialsPlayed) {
		gameNumber = gamesplayed;
		if (trialsPlayed == 0)
			trialNumber = "--";
		else
			trialNumber = "" + trialsPlayed;
		clearResults();
		int tmp = gamesplayed + 1;
		TextFile log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " no WRITENEWGAME falhou!");
			System.exit(1);
		}
		log.writelnString("Game " + tmp);
		if (!log.close()) {
			GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " no WRITENEWGAME falhou!");
			System.exit(1);
		}
		header();
	}

	public void writePosition(int[][] posRope, int team, int ct) {
		positionOnRope = posRope;
		String tmp = "" + posRope[team][ct];
		if (tmp.equals("-1"))
			ctPosOnRope[team][ct] = "-";
		else
			ctPosOnRope[team][ct] = tmp;
	}

	public int getPlaygroundId(int ct, int team) {
		for (int play = 0; play < Parameters.MAX_CT_ON_ROPE; play++) {
			if (positionOnRope[team][play] == ct) {
				return play;
			}
		}
		return -1;
	}

	public void writeMatchWinner(int[] gameScoreBoard) {
		TextFile log = new TextFile();
		if (!log.openForAppending(".", fileName)) {
			GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " no WRITEMATCHWINNER falhou!");
			System.exit(1);
		}
		if (gameScoreBoard[0] == gameScoreBoard[1]) {
			log.writelnString("Match was a draw.");
		} else if (gameScoreBoard[0] > gameScoreBoard[1]) {
			log.writelnString("Match was won by team 0 (" + gameScoreBoard[0] + "-" + gameScoreBoard[1] + ").");
		} else if (gameScoreBoard[0] < gameScoreBoard[1]) {
			log.writelnString("Match was won by team 1 (" + gameScoreBoard[0] + "-" + gameScoreBoard[1] + ").");
		}
		if (!log.close()) {
			GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " no WRITEMATCHWINNER falhou!");
			System.exit(1);
		}
	}

	private void clearResults() {
		trialNumber = "--";
		centerOftheRope = "--";
		for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
			for (int ct = 0; ct < Parameters.MAX_CT_ON_ROPE; ct++) {
				ctPosOnRope[team][ct] = "-";
			}
		}
	}

	public void writeTrialUpdate(int trialsPlayed, int ropeLocation, int gamesPlayed) {
		if (trialNumber == "--") {
			trialNumber = "--";
			centerOftheRope = "--";
		} else
			trialNumber = "" + trialsPlayed;
		centerOftheRope = "" + ropeLocation;
		gameNumber = gamesPlayed;
		if (matchFinished) {
			clearResults();
		}
	}

	public void updateCtStrenght(int team, int id, int strength) {
		contestantLogStrg[team][id] = strength;
	}

	private String getState(Object state) {
		if (state == ContestantState.DO_YOUR_BEST) {
			return "DYB";
		}
		if (state == ContestantState.SEAT_AT_BENCH) {
			return "SAB";
		}
		if (state == ContestantState.STAND_IN_POSITION) {
			return "POS";
		}
		if (state == ContestantState.SLEEP) {
			return "SLP";
		}
		if (state == CoachState.ASSEMBLE_TEAM) {
			return "ASST";
		}
		if (state == CoachState.SLEEP) {
			return "SLEP";
		}
		if (state == CoachState.WAIT_FOR_REFEREE_COMMAND) {
			return "WARC";
		}
		if (state == CoachState.WATCH_TRIAL) {
			return "WATR";
		}
		if (state == RefereeState.END_OF_A_GAME) {
			return "EGA";
		}
		if (state == RefereeState.END_OF_THE_MATCH) {
			return "EMA";
		}
		if (state == RefereeState.SLEEP) {
			return "SLP";
		}
		if (state == RefereeState.START_OF_A_GAME) {
			return "SGA";
		}
		if (state == RefereeState.START_OF_THE_MATCH) {
			return "SMA";
		}
		if (state == RefereeState.TEAMS_READY) {
			if (trialNumber == "--") {
				trialNumber = "1";
				centerOftheRope="0";
			}
			return "TRE";
		}
		if (state == RefereeState.WAIT_FOR_TRIAL_CONCLUSION) {
			return "WTR";
		}
		return null;
	}

}
