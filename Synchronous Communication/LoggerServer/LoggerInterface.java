/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoggerServer;

import Comms.Message;
import Comms.MessageException;
import Comms.Parameters;

/**
 *
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class LoggerInterface {

	private Logger logger;
	private volatile int shut;

	public LoggerInterface(Logger logger) {
		this.logger = logger;
	}

	public Message processAndReply(Message inMessage) throws MessageException {

		Message outMessage = null;

		// message received validation
		switch (inMessage.getMsgType()) {
		case Message.SETCNTSTATE:
			if (inMessage.getContestantId() == -1) {
				throw new MessageException("Dados de entrada de Contestant para alterar estado invalidos!", inMessage);
			}
			break;
		case Message.SETCOASTATE:
			if (inMessage.getCoachTeam() == -1) {
				throw new MessageException("Dados de entrada de Coach para alterar estado invalidos!", inMessage);
			}
			break;
		case Message.SETGAMEWINNERLOGGER:
			int[] tmp = inMessage.getScoreBoard();
			if (tmp[0] == -1 || tmp[1] == -1) {
				throw new MessageException(
						"Dados de entrada de Referee para o metodo Bench.AssertTrialDecision() invalidos!", inMessage);
			}
			break;
		case Message.DECLMATCHLOGGER:
			int[] tmp2 = inMessage.getScoreBoard();
			if (tmp2[0] == -1 || tmp2[1] == -1) {
				throw new MessageException(
						"Dados de entrada de Referee para o metodo Bench.AssertTrialDecision() invalidos!", inMessage);
			}
			break;
		case Message.WRITENEWGAMELOGGER:
			if (inMessage.getGamesPlayed() == -1 || inMessage.getTrialsPlayed() == -1) {
				throw new MessageException("Dados de entrada de Referee para o metodo AnnounceNewGame() invalidos!",
						inMessage);
			}
			break;
		}

		// Message precessed
		switch (inMessage.getMsgType()) {
		case Message.SETREFSTATE:
			logger.setrState(inMessage.getRefereeState());
			outMessage = new Message(Message.REFSTATESET);
			break;

		case Message.SETCNTSTATE:
			logger.setctState(inMessage.getContestantState(), inMessage.getContestantTeam(),
					inMessage.getContestantId());
			outMessage = new Message(Message.CNTSTATESET);
			break;

		case Message.SETCOASTATE:
			logger.setcState(inMessage.getCoachState(), inMessage.getCoachTeam());
			outMessage = new Message(Message.COASTATESET);
			break;

		case Message.SETGAMEWINNERLOGGER:
			logger.writeGameWinner(inMessage.getScoreBoard());
			outMessage = new Message(Message.GAMEWINNERSETLOGGER);
			break;
		case Message.DECLMATCHLOGGER:
			logger.writeMatchWinner(inMessage.getScoreBoard());
			outMessage = new Message(Message.MATCHDECLLOGGER);
			break;

		case Message.WRITENEWGAMELOGGER:
			logger.writeNewGame(inMessage.getGamesPlayed(), inMessage.getTrialsPlayed());
			outMessage = new Message(Message.NEWGAMEWROTELOGGER);
			break;

		case Message.WRITECTSTRENGTHLOGGER:
			logger.updateCtStrenght(inMessage.getContestantTeam(), inMessage.getContestantId(),
					inMessage.getContestantStrength());
			outMessage = new Message(Message.CTSTRENGTHLOGGERWRITEN);
			break;

		case Message.WRITETRIALUPDATELOGG:
			logger.writeTrialUpdate(inMessage.getTrialsPlayed(), inMessage.getCenterOfTheRope(),
					inMessage.getGamesPlayed());
			outMessage = new Message(Message.TRIALUPDATEDlOGG);
			break;

		case Message.UPDATEPOSLOGGER:
			logger.writePosition(inMessage.getPosOnRope(), inMessage.getContestantTeam(),
					inMessage.getcontestantPlaygroundId());
			outMessage = new Message(Message.POSUPDATEDLOGGER);
			break;
		case Message.SHUT: // shutdown do servidor
			shut++;
			if (shut == Parameters.SHUTDOWN) {
				LoggerMain.waitConnection = false;
				(((LoggerAps) (Thread.currentThread())).getScon()).setTimeout(10);
			}
			outMessage = new Message(Message.ACK); // gerar confirmação
			break;
		}
		return outMessage;
	}
}
