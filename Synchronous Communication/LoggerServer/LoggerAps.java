/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoggerServer;

import Comms.CoachState;
import Comms.ContestantState;
import Comms.Message;
import Comms.MessageException;
import Comms.RefereeState;
import Comms.Server_Com;
import genclass.GenericIO;
import java.util.logging.Level;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class LoggerAps extends Thread {

	private static int nProxy = 0;
	private Server_Com sconi;
	private LoggerInterface loggerInterface;

	private CoachState coachState;
	private int coachTeam;
	private String strategy;

	private ContestantState contestantState;
	private int contestantId, contestantTeam, strength, playgroundId;
	private boolean contestantSelected;

	private RefereeState refereeState;

	public LoggerAps(Server_Com sconi, LoggerInterface loggerInterface) {
		super("LoggerProxy_" + LoggerAps.getProxyId());

		this.sconi = sconi;
		this.loggerInterface = loggerInterface;
	}

	@Override
	public void run() {
		Message inMessage = null, // mensagem de entrada
				outMessage = null; // mensagem de saída
		String inString, outString;

		inString = (String) sconi.readObject(); // ler pedido do cliente
		inMessage = new Message(inString);

		try {
			outMessage = loggerInterface.processAndReply(inMessage); // processá-lo
		} catch (MessageException ex) {
			java.util.logging.Logger.getLogger(LoggerAps.class.getName()).log(Level.SEVERE, null, ex);
		}

		outString = outMessage.toXMLString();
		sconi.writeObject(outString); // enviar resposta ao cliente
		sconi.close(); // fechar canal de comunicação
	}

	private static int getProxyId() {
		Class<?> cl = null; // representação do tipo de dados ClientProxy na
							// máquina
		// virtual de Java
		int proxyId; // identificador da instanciação

		try {
			cl = Class.forName("LoggerServer.LoggerAps");
		} catch (ClassNotFoundException e) {
			GenericIO.writelnString("O tipo de dados ClientProxy não foi encontrado!");
			e.printStackTrace();
			System.exit(1);
		}

		synchronized (cl) {
			proxyId = nProxy;
			nProxy += 1;
		}

		return proxyId;
	}

	public Server_Com getScon() {
		return sconi;
	}

	// Coach gets/sets
	public CoachState getcState() {
		return coachState;
	}

	public void setcState(CoachState cState) {
		coachState = cState;
	}

	public int getTeam() {
		return coachTeam;
	}

	public String getStrategy() {
		return strategy;
	}

	// Contestant gets/sets
	public int getCtId() {
		return contestantId;
	}

	public int getStrength() {
		return strength;
	}

	public void gainStrength() {
		this.strength++;
	}

	public void loseStrength() {
		this.strength--;
	}

	public int getContestantTeam() {
		return contestantTeam;
	}

	public ContestantState getCtState() {
		return contestantState;
	}

	public void setCtState(ContestantState ctState) {
		this.contestantState = ctState;
	}

	public void setSelected(boolean flag) {
		contestantSelected = flag;
	}

	public boolean isSelected() {
		return contestantSelected;
	}

	public void setPlaygroundId(int playgroundId) {
		this.playgroundId = playgroundId;
	}

	public int getPlaygroundId() {
		return playgroundId;
	}

	// Referee gets/sets
	public RefereeState getrState() {
		return refereeState;
	}

	public void setrState(RefereeState rState) {
		this.refereeState = rState;
	}
}
