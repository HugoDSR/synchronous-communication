/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoggerServer;

import java.net.SocketTimeoutException;

import Comms.Parameters;
import Comms.Server_Com;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class LoggerMain {

	private static final int portNumb = Parameters.getportLoggerServer();
	public static boolean waitConnection = true;

	public static void main(String[] args) {

		Logger logger;
		LoggerInterface loggerInterface;

		Server_Com scon, sconi;
		LoggerAps loggerproxy;

		scon = new Server_Com(portNumb);
		scon.start();

		logger = new Logger();
		loggerInterface = new LoggerInterface(logger);

		System.out.println("O serviço foi establecido");
		System.out.println("O servidor esta em escuta");

		while (waitConnection) {
			try {
				sconi = scon.accept();
				loggerproxy = new LoggerAps(sconi, loggerInterface);
				loggerproxy.start();
			} catch (SocketTimeoutException e) {
			}

		}
		scon.end();
		System.out.println("O servidor foi desactivado.");
	}
}
