echo "Compressing data to be sent to the Logger Server node."
rm -rf loggerServer.zip
zip -rq loggerServer.zip LoggerServer Comms
echo "Transfering data to the Logger Server node."
sshpass -f password ssh cd0301@l040101-ws10.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws10.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp loggerServer.zip cd0301@l040101-ws10.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the Logger Server node."
sshpass -f password ssh cd0301@l040101-ws10.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q loggerServer.zip'
echo "Compiling program files at the Logger Server node."
sshpass -f password ssh cd0301@l040101-ws10.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the Logger Server node."
sshpass -f password ssh cd0301@l040101-ws10.ua.pt 'cd teste/TheRopeGameVDist ; java LoggerServer.LoggerMain'
