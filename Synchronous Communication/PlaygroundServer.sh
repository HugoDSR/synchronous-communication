echo "Compressing data to be sent to the Playground Server node."
rm -rf playgroundServer.zip
zip -rq playgroundServer.zip PlaygroundServer Comms
echo "Transfering data to the Playground Server node."
sshpass -f password ssh cd0301@l040101-ws08.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws08.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp playgroundServer.zip cd0301@l040101-ws08.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the Playground Server node."
sshpass -f password ssh cd0301@l040101-ws08.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q playgroundServer.zip'
echo "Compiling program files at the Playground Server node."
sshpass -f password ssh cd0301@l040101-ws08.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the Playground Server node."
sshpass -f password ssh cd0301@l040101-ws08.ua.pt 'cd teste/TheRopeGameVDist ; java PlaygroundServer.PlaygroundMain'
