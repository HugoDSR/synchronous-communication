echo "Compressing data to be sent to the Referee Client node."
rm -rf refereeClient.zip
zip -rq refereeClient.zip RefereeClient Comms
echo "Transfering data to the Referee Client node."
sshpass -f password ssh cd0301@l040101-ws06.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws06.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp refereeClient.zip cd0301@l040101-ws06.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the Referee Client node."
sshpass -f password ssh cd0301@l040101-ws06.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q refereeClient.zip'
echo "Compiling program files at the Referee Client node."
sshpass -f password ssh cd0301@l040101-ws06.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the Referee Client node."
sshpass -f password ssh cd0301@l040101-ws06.ua.pt 'cd teste/TheRopeGameVDist ; java RefereeClient.RefereeMain'
