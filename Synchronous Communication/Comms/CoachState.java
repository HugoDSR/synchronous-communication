/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comms;

/**
*
* @author Hugo Rocha nmec 68511
* @author Miguel Antunes nmec 68484
*/
public enum CoachState {
         SLEEP,
         WAIT_FOR_REFEREE_COMMAND,
         ASSEMBLE_TEAM,
         WATCH_TRIAL;
}
