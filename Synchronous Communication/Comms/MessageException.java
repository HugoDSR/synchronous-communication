/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comms;
/**
*
* @author Hugo Rocha nmec 68511
* @author Miguel Antunes nmec 68484
*/
public class MessageException extends Exception{
         
         /**
          * Message that originated exception
          * 
          * @serialField message
          */
         private Message message;
         
         /**
          * Message instantiation
          * 
          * @param errorMessage error condition text
          * @param message message that originated exception
          */
         
         public MessageException (String errorMessage, Message message){
                  super(errorMessage);
                  this.message=message;
         }
         
         /**
          * Get message
          * 
          * @return message;
          */
         
         public Message getMensagemVal(){
                  return (message);
         }
         
}
