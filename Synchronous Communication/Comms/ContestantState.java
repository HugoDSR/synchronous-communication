/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comms;

/**
*
* @author Hugo Rocha nmec 68511
* @author Miguel Antunes nmec 68484
*/
public enum ContestantState {
         SLEEP,
         SEAT_AT_BENCH,
         STAND_IN_POSITION,
         DO_YOUR_BEST;
}
