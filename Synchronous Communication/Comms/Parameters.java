package Comms;

/**
 *
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */

public class Parameters {

	public static int MAX_CONTESTANTS = 5, MAX_TEAMS = 2, MAX_COACHES = 2, MAX_REFEREES = 1, MAX_CT_ON_ROPE = 3,
			MAX_TRIALS = 6, MAX_GAMES = 3, KNOCKOUT = 4, SHUTDOWN = 3;

	private static final int portBenchServer = 22707;
	private static final String hostNameBenchServer = "localhost";// "l040101-ws07";

	private static final int portPlaygroundServer = 22708;
	private static final String hostNamePlaygroundServer = "localhost";// "l040101-ws08";

	private static final int portRefereeSiteServer = 22709;
	private static final String hostNameRefereeSiteServer = "localhost";// "l040101-ws09";

	private static final int portLoggerServer = 22700;
	private static final String hostNameLoggerServer = "localhost";// "l040101-ws10";

	public static int getportBenchServer() {
		return portBenchServer;
	}

	public static String gethostNameBenchServer() {
		return hostNameBenchServer;
	}

	public static int getportPlaygroundServer() {
		return portPlaygroundServer;
	}

	public static String gethostNamePlaygroundServer() {
		return hostNamePlaygroundServer;
	}

	public static int getportRefereeSiteServer() {
		return portRefereeSiteServer;
	}

	public static String gethostNameRefereeSiteServer() {
		return hostNameRefereeSiteServer;
	}

	public static int getportLoggerServer() {
		return portLoggerServer;
	}

	public static String gethostNameLoggerServer() {
		return hostNameLoggerServer;
	}

}
