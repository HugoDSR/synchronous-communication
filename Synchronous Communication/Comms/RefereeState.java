/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comms;

/**
*
* @author Hugo Rocha nmec 68511
* @author Miguel Antunes nmec 68484
*/
public enum RefereeState {
         SLEEP,
         START_OF_THE_MATCH,
         START_OF_A_GAME,
         TEAMS_READY,
         WAIT_FOR_TRIAL_CONCLUSION,
         END_OF_A_GAME,
         END_OF_THE_MATCH;
}
