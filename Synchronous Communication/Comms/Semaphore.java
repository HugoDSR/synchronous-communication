package Comms;

public class Semaphore {

         private int val = 0, numBlockThreads = 0;

         public synchronized void down() {
                  if (val == 0) {
                           numBlockThreads += 1;
                           try {
                                    wait();
                           } catch (InterruptedException e) {
                           }
                  } else {
                           val -= 1;
                  }
         }

         public synchronized void up() {
                  if (numBlockThreads != 0) {
                           numBlockThreads -= 1;
                           notify();
                  } else {
                           val += 1;
                  }
         }

}
