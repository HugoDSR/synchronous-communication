/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comms;

import java.io.*;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

import genclass.GenericIO;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class Message implements Serializable {

	/**
	 * Serialization key
	 *
	 * @serialField serialVersionUID
	 */
	private static final long serialVersionUID = 1111L;

	// Message type

	/**
	 * REFEREE ASSOCIATED MESSAGES
	 */
	/**
	 * Referee announces new game (requested by client)
	 *
	 * @serialField ANGAME
	 */
	public static final int ANGAME = 3;

	/**
	 * Game has been initiated (reply from server)
	 *
	 * @serialField GAMESTARTED
	 */
	public static final int GAMESTARTED = 4;

	/**
	 * Referee calls a new Trial (requested by client)
	 *
	 * @serialField CALLTRIAL
	 */
	public static final int CALLTRIAL = 5;

	/**
	 * Trial has been called (reply from server)
	 *
	 * @serialField TRIALCALLED
	 */
	public static final int TRIALCALLED = 6;

	/**
	 * Referee starts a new trial (requested by client)
	 *
	 * @serialField STARTTRIAL
	 */
	public static final int STARTTRIAL = 7;

	/**
	 * Trial has been started (reply from server)
	 *
	 * @serialField TRIALSTARTED
	 */
	public static final int TRIALSTARTED = 8;

	/**
	 * Referee asserts trials decision (requested by client)
	 *
	 * @serialField ASSERTTRIAL
	 */
	public static final int ASSERTTRIAL = 9;

	/**
	 * Trial results asserted (reply from server)
	 *
	 * @serialField TRIALASSERTED
	 */
	public static final int TRIALASSERTED = 10;

	/**
	 * Referee declare game winner (requested by client)
	 *
	 * @serialField GAMEWINNER
	 */
	public static final int GAMEWINNER = 11;

	/**
	 * Game winner declared (reply from server)
	 *
	 * @serialField GAMEWINNERDECL
	 */
	public static final int GAMEWINNERDECL = 12;

	/**
	 * Referee declare match winner (requested by client)
	 *
	 * @serialField MATCHWINNER
	 */
	public static final int MATCHWINNER = 13;

	/**
	 * Match winner declared (reply from server)
	 *
	 * @serialField MATCHWINNERDECL
	 */
	public static final int MATCHWINNERDECL = 14;

	/**
	 * Referee asks for Game scoreboard(requested by client)
	 *
	 * @serialField GETSCOREBOARD
	 */
	public static final int GETSCOREBOARD = 35;

	/**
	 * Game scoreboard delivered (reply from server)
	 *
	 * @serialField SCOREBOARDDELIV
	 */
	public static final int SCOREBOARDDELIV = 36;

	/**
	 * Referee asks for Game number(requested by client)
	 *
	 * @serialField GETGAMENUM
	 */
	public static final int GETGAMENUM = 37;

	/**
	 * Game number delivered (reply from server)
	 *
	 * @serialField GAMENUMDELIV
	 */
	public static final int GAMENUMDELIV = 38;

	/**
	 * Referee wakes up bench(requested by client)
	 *
	 * @serialField REFWAKEUPBENCH
	 */
	public static final int REFWAKEUPBENCH = 41;

	/**
	 * bench woken up by referee (reply from server)
	 *
	 * @serialField BENCHWOKENUP
	 */
	public static final int BENCHWOKENUP = 42;

	/**
	 * Referee wakes up contestants(requested by client)
	 *
	 * @serialField REFWAKEUPCONTEST
	 */
	public static final int REFWAKEUPCONTEST = 43;

	/**
	 * contestants woken up by referee (reply from server)
	 *
	 * @serialField CONTESTWOKENUP
	 */
	public static final int CONTESTWOKENUP = 44;

	/**
	 * Referee wakes up coaches on bench(requested by client)
	 *
	 * @serialField REFWAKEUPCOAHESB
	 */
	public static final int REFWAKEUPCOAHESB = 45;

	/**
	 * coaches on bench woken up by referee (reply from server)
	 *
	 * @serialField COAHESWOKENUPB
	 */
	public static final int COAHESWOKENUPB = 46;

	/**
	 * Referee ends game(requested by client)
	 *
	 * @serialField REFENDSGAME
	 */
	public static final int REFENDSGAME = 47;

	/**
	 * game ended by referee (reply from server)
	 *
	 * @serialField GAMEENDED
	 */
	public static final int GAMEENDED = 48;

	/**
	 * Referee wakes up coaches on playground(requested by client)
	 *
	 * @serialField REFWAKEUPCOAHESP
	 */
	public static final int REFWAKEUPCOAHESP = 49;

	/**
	 * coaches on playground woken up by referee (reply from server)
	 *
	 * @serialField COAHESWOKENUPP
	 */
	public static final int COAHESWOKENUPP = 50;

	/**
	 * Referee clears ScoreBoard on playground(requested by client)
	 *
	 * @serialField REFCLEARSCORE
	 */
	public static final int REFCLEARSCORE = 53;

	/**
	 * ScoreBoard on playground is cleared up by referee (reply from server)
	 *
	 * @serialField SCORECLEAR
	 */
	public static final int SCORECLEAR = 54;

	/**
	 * CONTESTANT ASSOCIATED MESSAGES
	 */

	/**
	 * Contestants seats down (request by client)
	 *
	 * @serialField CNTSEAT
	 */
	public static final int CNTSEAT = 17;

	/**
	 * Seated down contestant (reply from server)
	 *
	 * @serialField SEATEDCONT
	 */
	public static final int SEATEDCONT = 18;

	/**
	 * Contestants follow the coach advice (request by client)
	 *
	 * @serialField CNTFOLLOW
	 */
	public static final int CNTFOLLOW = 19;

	/**
	 * Coach advice was followed (reply from server)
	 *
	 * @serialField FOLLOWEDADV
	 */
	public static final int FOLLOWEDADV = 20;

	/**
	 * Contestants gets ready on rope (request by client)
	 *
	 * @serialField CNTGETREADY
	 */
	public static final int CNTGETREADY = 21;

	/**
	 * Contestant is ready on rope (reply from server)
	 *
	 * @serialField CNTISREADY
	 */
	public static final int CNTISREADY = 22;

	/**
	 * Contestants is done in attempt (request by client)
	 *
	 * @serialField CNTDONE
	 */
	public static final int CNTDONE = 23;

	/**
	 * Contestant done acknowledge (reply from server)
	 *
	 * @serialField CNTDONEACKN
	 */
	public static final int CNTDONEACKN = 24;

	/**
	 * COACH ASSOCIATED MESSAGES
	 */

	/**
	 * Coach reviews notes for contestants (requested by client)
	 *
	 * @serialField COAREVNOTES
	 */
	public static final int COAREVNOTES = 27;

	/**
	 * Coach notes were reviewed (reply from server)
	 *
	 * @serialField COANOTESREV
	 */
	public static final int COANOTESREV = 28;

	/**
	 * Coach selected contestants playground(requested by client)
	 *
	 * @serialField COACALLSCONT
	 */
	public static final int COACALLSCONT = 29;

	/**
	 * Coach contestants for playground selected(reply from server)
	 *
	 * @serialField CONTCALLED
	 */
	public static final int CONTCALLED = 30;

	/**
	 * Coach informs referee that team is ready to play(requested by client)
	 *
	 * @serialField COACHINFREF
	 */
	public static final int COACHINFREF = 31;

	/**
	 * Coach informed referee that team is ready to play (reply from server)
	 *
	 * @serialField COACHINFORMEDREF
	 */
	public static final int COACHINFORMEDREF = 32;

	/**
	 * Logger ASSOCIATED MESSAGES
	 */

	/**
	 * Referee asks to change state(requested by client)
	 *
	 * @serialField SETREFSTATE
	 */
	public static final int SETREFSTATE = 15;

	/**
	 * State of referee changed (reply from server)
	 *
	 * @serialField REFSTATESET
	 */
	public static final int REFSTATESET = 16;

	/**
	 * Contestant asks to change state(requested by client)
	 *
	 * @serialField SETCNTSTATE
	 */
	public static final int SETCNTSTATE = 25;

	/**
	 * State of Contestant changed (reply from server)
	 *
	 * @serialField CNTSTATESET
	 */
	public static final int CNTSTATESET = 26;

	/**
	 * COACH asks to change state(requested by client)
	 *
	 * @serialField SETCOASTATE
	 */
	public static final int SETCOASTATE = 33;

	/**
	 * State of COACH changed (reply from server)
	 *
	 * @serialField COASTATESET
	 */
	public static final int COASTATESET = 34;

	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField SETGAMEWINNERLOGGER
	 */
	public static final int SETGAMEWINNERLOGGER = 39;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField GAMEWINNERSETLOGGER
	 */
	public static final int GAMEWINNERSETLOGGER = 40;
	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField WRITENEWGAMELOGGER
	 */
	public static final int WRITENEWGAMELOGGER = 51;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField NEWGAMEWROTELOGGER
	 */
	public static final int NEWGAMEWROTELOGGER = 52;
	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField WRITECTSTRENGTHLOGGER
	 */
	public static final int WRITECTSTRENGTHLOGGER = 55;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField CTSTRENGTHLOGGERWRITEN
	 */
	public static final int CTSTRENGTHLOGGERWRITEN = 56;
	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField WRITETRIALUPDATELOGG
	 */
	public static final int WRITETRIALUPDATELOGG = 57;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField TRIALUPDATEDlOGG
	 */
	public static final int TRIALUPDATEDlOGG = 58;
	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField WRITETRIALUPDATELOGG
	 */
	public static final int UPDATEPOSLOGGER = 59;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField TRIALUPDATEDlOGG
	 */
	public static final int POSUPDATEDLOGGER = 60;
	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField WRITETRIALUPDATELOGG
	 */
	public static final int SHUT = 61;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField TRIALUPDATEDlOGG
	 */
	public static final int ACK = 62;
	/**
	 * Referee asks logger to set Game Scores(requested by client)
	 *
	 * @serialField WRITETRIALUPDATELOGG
	 */
	public static final int DECLMATCHLOGGER = 63;

	/**
	 * Game Score setted by the Logger (reply from server)
	 *
	 * @serialField TRIALUPDATEDlOGG
	 */
	public static final int MATCHDECLLOGGER = 64;

	/**
	 * Entities States Entities values
	 */
	private boolean gameEnded;
	private int[][] posRope = new int[2][3];
	private int contestantId = -1;
	private double matchPlayed = -1;

	private int contestantTeam = -1;
	private int contestantStrength = -1;
	private int contestantPlaygroundId = -1;

	private int gamesPlayed = -1;
	private int trialsPlayed = -1;

	private int centerOfTheRope;

	private int coachTeam = -1;
	private String coachStrategy = "";

	private int[] scoreBoard = new int[2];
	private CoachState coachState;
	private ContestantState contestantState;
	private RefereeState refereeState;

	/* Message Fields */
	/**
	 * Message type
	 *
	 * @serialField msgType
	 */
	private int msgType = -1;

	/* Difrent Message instantiation methods */
	public Message(int type) {
		msgType = type;
	}

	public Message(int type, boolean gameEnded) {
		msgType = type;
		this.gameEnded = gameEnded;
	}

	public Message(int type, int id, CoachState coachState) {
		msgType = type;
		coachTeam = id;
		this.coachState = coachState;
	}

	public Message(int type, RefereeState refereeState) {
		msgType = type;
		this.refereeState = refereeState;
	}

	public Message(int type, int teamId, int id, ContestantState contestantState) {
		msgType = type;
		contestantTeam = teamId;
		contestantId = id;
		this.contestantState = contestantState;
	}

	public Message(int type, int val1, int val2) {
		msgType = type;
		if (type == 20) {
			contestantPlaygroundId = val1;
			contestantStrength = val2;
		}
		if (type == 39) {
		}
		if (type == 51) {
			gamesPlayed = val1;
			trialsPlayed = val2;
		} else {
			contestantTeam = val1;
			contestantId = val2;
		}
	}

	public Message(int type, int val1, int val2, int val3) {
		msgType = type;
		if (type == WRITETRIALUPDATELOGG) {
			trialsPlayed = val1;
			centerOfTheRope = val2;
			gamesPlayed = val3;

		} else {
			contestantId = val2;
			contestantTeam = val1;
			contestantStrength = val3;
		}
	}

	public Message(int type, int teamId) {
		msgType = type;
		coachTeam = teamId;
	}

	public Message(int type, int teamId, String strategy) {
		msgType = type;
		coachTeam = teamId;
		coachStrategy = strategy;
	}

	public Message(int type, int[] scoreBoard) {
		msgType = type;
		this.scoreBoard = scoreBoard;
	}

	public Message(int type, double matchPlayed) {
		msgType = type;
		this.matchPlayed = matchPlayed;
	}

	public Message(int type, int teamId, int cntId, int playgroundId, int strength) {
		msgType = type;
		contestantTeam = teamId;
		contestantId = cntId;
		contestantPlaygroundId = playgroundId;
		contestantStrength = strength;
	}

	public Message(int type, int[][] posRope, int contestantTeam, int playgroundId) {
		msgType = type;
		contestantPlaygroundId = playgroundId;
		this.contestantTeam = contestantTeam;
		this.posRope = posRope;
	}

	/**
	 * Getters
	 */
	public int getMsgType() {
		return msgType;
	}

	/* Contestant */
	public int getContestantId() {
		return contestantId;
	}

	public int getCenterOfTheRope() {
		return centerOfTheRope;
	}

	public int getcontestantPlaygroundId() {
		return contestantPlaygroundId;
	}

	public int getContestantTeam() {
		return contestantTeam;
	}

	public int getContestantStrength() {
		return contestantStrength;
	}

	public ContestantState getContestantState() {
		return contestantState;
	}

	public int[][] getPosOnRope() {
		return posRope;
	}

	/* Coach */
	public int getCoachTeam() {
		return coachTeam;
	}

	public String getCoachStrategy() {
		return coachStrategy;
	}

	public CoachState getCoachState() {
		return coachState;
	}

	/* Referee */
	public RefereeState getRefereeState() {
		return refereeState;
	}

	public int[] getScoreBoard() {
		return scoreBoard;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public int getTrialsPlayed() {
		return trialsPlayed;
	}

	public boolean hasgameEnded() {
		return gameEnded;
	}

	public double getmatchPlayed() {
		return matchPlayed;
	}

	public String toXMLString() {

		String xml = "";
		String strType = "";

		// ----------------------------------//
		// Falta a validação das mensagens //
		// ----------------------------------//

		switch (getMsgType()) {

		case ANGAME: // 3
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case GAMESTARTED:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<gameEnded>" + gameEnded + "</gameEnded>");
			break;
		case CALLTRIAL:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case TRIALCALLED:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case STARTTRIAL:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case TRIALSTARTED:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case ASSERTTRIAL:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case TRIALASSERTED:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case GAMEWINNER:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case GAMEWINNERDECL:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case MATCHWINNER:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case MATCHWINNERDECL:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SETREFSTATE:
			strType = new String(
					"<Tipo>" + msgType + "</Tipo>" + "<EstadoRef>" + refereeState.toString() + "</EstadoRef>");
			break;
		case REFSTATESET:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case CNTSEAT:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantTeam>" + contestantTeam
					+ "</contestantTeam>" + "<contestantId>" + contestantId + "</contestantId>" + "<contestantStrength>"
					+ contestantStrength + "</contestantStrength>");
			break;
		case SEATEDCONT:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<gameEnded>" + gameEnded + "</gameEnded>");
			break;
		case CNTFOLLOW:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantId>" + contestantId + "</contestantId>"
					+ "<contestantTeam>" + contestantTeam + "</contestantTeam>" + "<contestantPlaygroundId>"
					+ contestantPlaygroundId + "</contestantPlaygroundId>" + "<contestantStrength>" + contestantStrength
					+ "</contestantStrength>");
			break;
		case FOLLOWEDADV:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantPlaygroundId>" + contestantPlaygroundId
					+ "</contestantPlaygroundId>" + "<contestantStrength>" + contestantStrength
					+ "</contestantStrength>");
			break;
		case CNTGETREADY:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantTeam>" + contestantTeam
					+ "</contestantTeam>" + "<contestantId>" + contestantId + "</contestantId>");
			break;
		case CNTISREADY:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case CNTDONE:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantTeam>" + contestantTeam
					+ "</contestantTeam>" + "<contestantId>" + contestantId + "</contestantId>");
			break;
		case CNTDONEACKN:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SETCNTSTATE:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantTeam>" + contestantTeam
					+ "</contestantTeam>" + "<contestantId>" + contestantId + "</contestantId>" + "<EstadoCnt>"
					+ contestantState.toString() + "</EstadoCnt>");
			break;
		case CNTSTATESET:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case COAREVNOTES:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<coachTeam>" + coachTeam + "</coachTeam>");
			break;
		case COANOTESREV:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<gameEnded>" + gameEnded + "</gameEnded>");
			break;
		case COACALLSCONT:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<coachTeam>" + coachTeam + "</coachTeam>"
					+ "<strategy>" + coachStrategy + "</strategy>");
			break;
		case CONTCALLED:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case COACHINFREF:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<coachTeam>" + coachTeam + "</coachTeam>");
			break;
		case COACHINFORMEDREF:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SETCOASTATE:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<coachTeam>" + coachTeam + "</coachTeam>"
					+ "<EstadoCoa>" + coachState.toString() + "</EstadoCoa>");
			break;
		case COASTATESET:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case GETSCOREBOARD:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SCOREBOARDDELIV:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<scoreBoard>" + scoreBoard[0] + "," + scoreBoard[1]
					+ "</scoreBoard>");
			break;
		case GETGAMENUM:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case GAMENUMDELIV:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SETGAMEWINNERLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<scoreBoard>" + scoreBoard[0] + "," + scoreBoard[1]
					+ "</scoreBoard>");
			break;
		case GAMEWINNERSETLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case REFWAKEUPBENCH:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case BENCHWOKENUP:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case REFWAKEUPCONTEST:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case CONTESTWOKENUP:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case REFWAKEUPCOAHESB:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case COAHESWOKENUPB:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case REFENDSGAME:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<gameEnded>" + gameEnded + "</gameEnded>");
			break;
		case GAMEENDED:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case REFWAKEUPCOAHESP:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case COAHESWOKENUPP:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case WRITENEWGAMELOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<gamesPlayed>" + gamesPlayed + "</gamesPlayed>"
					+ "<trialsPlayed>" + trialsPlayed + "</trialsPlayed>");
			break;
		case NEWGAMEWROTELOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case REFCLEARSCORE:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SCORECLEAR:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case WRITECTSTRENGTHLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<contestantTeam>" + contestantTeam
					+ "</contestantTeam>" + "<contestantId>" + contestantId + "</contestantId>" + "<contestantStrength>"
					+ contestantStrength + "</contestantStrength>");
			break;
		case CTSTRENGTHLOGGERWRITEN:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case WRITETRIALUPDATELOGG:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<trialsPlayed>" + trialsPlayed + "</trialsPlayed>"
					+ "<centerOfTheRope>" + centerOfTheRope + "</centerOfTheRope>" + "<gamesPlayed>" + gamesPlayed
					+ "</gamesPlayed>");
			break;
		case TRIALUPDATEDlOGG:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case UPDATEPOSLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<posRope1>" + posRope[0][0] + "," + posRope[1][0]
					+ "</posRope1>" + "<posRope2>" + posRope[0][1] + "," + posRope[1][1] + "</posRope2>" + "<posRope3>"
					+ posRope[0][2] + "," + posRope[1][2] + "</posRope3>" + "<contestantTeam>" + contestantTeam
					+ "</contestantTeam>" + "<contestantPlaygroundId>" + contestantPlaygroundId
					+ "</contestantPlaygroundId>");
			break;
		case POSUPDATEDLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case SHUT:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case ACK:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		case DECLMATCHLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>" + "<scoreBoard>" + scoreBoard[0] + "," + scoreBoard[1]
					+ "</scoreBoard>");
			break;
		case MATCHDECLLOGGER:
			strType = new String("<Tipo>" + msgType + "</Tipo>");
			break;
		}
		return ("<Mensagem>" + strType + "</Mensagem>");
	}

	public Message(String stringXML) {
		InputSource in = new InputSource(new StringReader(stringXML));
		SAXParserFactory spf;
		SAXParser saxParser = null;

		spf = SAXParserFactory.newInstance();
		spf.setNamespaceAware(false);
		spf.setValidating(false);
		try {
			saxParser = spf.newSAXParser();
			saxParser.parse(in, new HandlerXML());
		} catch (ParserConfigurationException e) {
			GenericIO.writelnString("Erro na instanciação do parser (configuração): " + e.getMessage() + "!");
			System.exit(1);
		} catch (SAXException e) {
			GenericIO.writelnString("Erro na instanciação do parser (SAX): " + e.getMessage() + "!");
			System.exit(1);
		} catch (IOException e) {
			GenericIO.writelnString("Erro na execução do parser (SAX): " + e.getMessage() + "!");
			System.exit(1);
		}
	}

	private class HandlerXML extends DefaultHandler {
		/**
		 * Parsing do string XML em curso
		 * 
		 * @serialField parsing
		 */

		private boolean parsing;

		/**
		 * Parsing de um elemento em curso
		 * 
		 * @serialField element
		 */

		private boolean element;

		/**
		 * Nome do elemento em processamento
		 * 
		 * @serialField elemName
		 */

		private String elemName;

		/**
		 * Início do processamento do string XML.
		 *
		 */

		@Override
		public void startDocument() throws SAXException {
			msgType = -1;
			contestantId = -1;
			matchPlayed = -1;
			contestantTeam = -1;
			contestantStrength = -1;
			contestantPlaygroundId = -1;
			gamesPlayed = -1;
			trialsPlayed = -1;
			centerOfTheRope = -1;
			coachTeam = -1;
			coachStrategy = "";
			scoreBoard[0] = -1;
			scoreBoard[1] = -1;
			parsing = true;
		}

		/**
		 * Fim do processamento do string XML.
		 *
		 */

		@Override
		public void endDocument() throws SAXException {
			parsing = false;
		}

		/**
		 * Início do processamento de um elemento do string XML.
		 *
		 */

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
				throws SAXException {
			element = parsing;
			if (parsing)
				elemName = qName;
		}

		/**
		 * Fim do processamento de um elemento do string XML.
		 *
		 */

		@Override
		public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
			element = false;
			elemName = null;
		}

		/**
		 * Processamento do conteúdo de um elemento do string XML.
		 *
		 */

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String elem;

			elem = new String(ch, start, length);
			if (parsing && element) {
				if (elemName.equals("Tipo")) {
					msgType = new Integer(elem);
					return;
				}
				if (elemName.equals("EstadoRef")) {
					if (new String(elem).equalsIgnoreCase("END_OF_A_GAME"))
						refereeState = RefereeState.END_OF_A_GAME;
					if (new String(elem).equalsIgnoreCase("END_OF_THE_MATCH"))
						refereeState = RefereeState.END_OF_THE_MATCH;
					if (new String(elem).equalsIgnoreCase("SLEEP"))
						refereeState = RefereeState.SLEEP;
					if (new String(elem).equalsIgnoreCase("START_OF_A_GAME"))
						refereeState = RefereeState.START_OF_A_GAME;
					if (new String(elem).equalsIgnoreCase("START_OF_THE_MATCH"))
						refereeState = RefereeState.START_OF_THE_MATCH;
					if (new String(elem).equalsIgnoreCase("TEAMS_READY"))
						refereeState = RefereeState.TEAMS_READY;
					if (new String(elem).equalsIgnoreCase("WAIT_FOR_TRIAL_CONCLUSION"))
						refereeState = RefereeState.WAIT_FOR_TRIAL_CONCLUSION;
					return;
				}
				if (elemName.equals("EstadoCnt")) {
					if (new String(elem).equalsIgnoreCase("DO_YOUR_BEST"))
						contestantState = ContestantState.DO_YOUR_BEST;
					if (new String(elem).equalsIgnoreCase("SEAT_AT_BENCH"))
						contestantState = ContestantState.SEAT_AT_BENCH;
					if (new String(elem).equalsIgnoreCase("SLEEP"))
						contestantState = ContestantState.SLEEP;
					if (new String(elem).equalsIgnoreCase("STAND_IN_POSITION"))
						contestantState = ContestantState.STAND_IN_POSITION;
					return;
				}
				if (elemName.equals("EstadoCOA")) {
					if (new String(elem).equalsIgnoreCase("ASSEMBLE_TEAM"))
						coachState = CoachState.ASSEMBLE_TEAM;
					if (new String(elem).equalsIgnoreCase("SLEEP"))
						coachState = CoachState.SLEEP;
					if (new String(elem).equalsIgnoreCase("WAIT_FOR_REFEREE_COMMAND"))
						coachState = CoachState.WAIT_FOR_REFEREE_COMMAND;
					if (new String(elem).equalsIgnoreCase("WATCH_TRIAL"))
						coachState = CoachState.WATCH_TRIAL;
					return;
				}
				if (elemName.equals("contestantTeam")) {
					contestantTeam = new Integer(elem);
					return;
				}
				if (elemName.equals("strategy")) {
					coachStrategy = new String(elem);
				}
				if (elemName.equals("contestantId")) {
					contestantId = new Integer(elem);
					return;
				}
				if (elemName.equals("contestantStrength")) {
					contestantStrength = new Integer(elem);
					return;
				}
				if (elemName.equals("coachTeam")) {
					coachTeam = new Integer(elem);
					return;
				}
				if (elemName.equals("coachStrategy")) {
					coachStrategy = new String(elem);
					return;
				}

				if (elemName.equals("scoreBoard")) {
					String[] tmp = new String(elem).split(",");
					scoreBoard[0] = new Integer(tmp[0]);
					scoreBoard[1] = new Integer(tmp[1]);
					return;
				}
				if (elemName.equals("gamesPlayed")) {
					gamesPlayed = new Integer(elem);
					return;
				}
				if (elemName.equals("trialsPlayed")) {
					trialsPlayed = new Integer(elem);
					return;
				}
				if (elemName.equals("centerOfTheRope")) {
					centerOfTheRope = new Integer(elem);
					return;
				}
				if (elemName.equals("posRope1")) {
					String[] tmp = new String(elem).split(",");
					posRope[0][0] = new Integer(tmp[0]);
					posRope[1][0] = new Integer(tmp[1]);
					return;
				}
				if (elemName.equals("posRope2")) {
					String[] tmp = new String(elem).split(",");
					posRope[0][1] = new Integer(tmp[0]);
					posRope[1][1] = new Integer(tmp[1]);
					return;
				}
				if (elemName.equals("posRope3")) {
					String[] tmp = new String(elem).split(",");
					posRope[0][2] = new Integer(tmp[0]);
					posRope[1][2] = new Integer(tmp[1]);
					return;
				}
				if (elemName.equals("contestantPlaygroundId")) {
					contestantPlaygroundId = new Integer(elem);
					return;
				}
				if (elemName.equals("gameEnded")) {
					gameEnded = new Boolean(elem);
				}
			}
		}
	}
}
