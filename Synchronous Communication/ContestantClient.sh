echo "Compressing data to be sent to the Contestant Client node."
rm -rf contestantClient.zip
zip -rq contestantClient.zip ContestantClient Comms
echo "Transfering data to the Contestant Client node."
sshpass -f password ssh cd0301@l040101-ws04.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws04.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp contestantClient.zip cd0301@l040101-ws04.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the Contestant Client node."+
sshpass -f password ssh cd0301@l040101-ws04.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q contestantClient.zip'
echo "Compiling program files at the Contestant Client node."
sshpass -f password ssh cd0301@l040101-ws04.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the Coach Contestant node."
sshpass -f password ssh cd0301@l040101-ws04.ua.pt 'cd teste/TheRopeGameVDist ; java ContestantClient.ContestantMain'
