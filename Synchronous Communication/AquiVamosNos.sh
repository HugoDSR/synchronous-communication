xterm  -T "Bench Side" -hold -e "./LocalBenchServer.sh" &
xterm  -T "Coach Side" -hold -e "./LocalCoachClient.sh" &
xterm  -T "Contestant Side" -hold -e "./LocalContestantClient.sh" &
xterm  -T "Logger Side" -hold -e "./LocalLoggerServer.sh" &
xterm  -T "Playground Side" -hold -e "./LocalPlaygroundServer.sh" &
xterm  -T "Referee Side" -hold -e "./LocalRefereeClient.sh" &
xterm  -T "RefereeSite Side" -hold -e "./LocalRefereeSiteServer.sh" &
