xterm  -T "BenchServer Side" -hold -e "./BenchServer.sh" &
xterm  -T "PlaygroundServer Side" -hold -e "./PlaygroundServer.sh" &
xterm  -T "RefereeSiteServer Side" -hold -e "./RefereeSiteServer.sh" &
xterm  -T "LoggerServer Side" -hold -e "./LoggerServer.sh" &
xterm  -T "ContestantClient Side" -hold -e "./ContestantClient.sh" &
xterm  -T "CoachClient Side" -hold -e "./CoachClient.sh" &
xterm  -T "RefereeClient Side" -hold -e "./RefereeClient.sh" &
-
