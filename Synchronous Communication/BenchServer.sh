echo "Compressing data to be sent to the Bench Server node."
rm -rf benchServer.zip
zip -rq benchServer.zip BenchServer Comms
echo "Transfering data to the Bench Server node."
sshpass -f password ssh cd0301@l040101-ws07.ua.pt 'mkdir -p teste/TheRopeGameVDist'
sshpass -f password ssh cd0301@l040101-ws07.ua.pt 'rm -rf teste/TheRopeGameVDist/*'
sshpass -f password scp benchServer.zip cd0301@l040101-ws07.ua.pt:teste/TheRopeGameVDist
echo "Decompressing data sent to the Bench Server node."
sshpass -f password ssh cd0301@l040101-ws07.ua.pt 'cd teste/TheRopeGameVDist ; unzip -q benchServer.zip'
echo "Compiling program files at the Bench Server node."
sshpass -f password ssh cd0301@l040101-ws07.ua.pt 'cd teste/TheRopeGameVDist ; javac */*.java'
sleep 3
echo "Executing program at the Bench Server node."
sshpass -f password ssh cd0301@l040101-ws07.ua.pt 'cd teste/TheRopeGameVDist ; java BenchServer.MainBench'
