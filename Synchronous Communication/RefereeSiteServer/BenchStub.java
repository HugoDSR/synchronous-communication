package RefereeSiteServer;

import Comms.Client_Com;
import Comms.Message;
import genclass.GenericIO;

/**
 *
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class BenchStub {

	/**
	 * Name of computational sister where server is located
	 *
	 * @SerialField serverHostName
	 */
	private String serverHostName;

	/**
	 * Number of server listening port
	 *
	 * @SerialField serverPortNumber
	 */
	private int serverPortNumber;

	/**
	 * BenchStub Instantiation
	 *
	 * @param hostname
	 *            name of the system where server is located
	 * @param port
	 *            number of server listening port
	 *
	 */
	public BenchStub(String hostname, int port) {
		serverHostName = hostname;
		serverPortNumber = port;
	}

	public void endGame() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.REFENDSGAME);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);
		if (inMessage.getMsgType() != Message.GAMEENDED) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();

	}

	public void refWakeUpBench() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.REFWAKEUPBENCH);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);
		if (inMessage.getMsgType() != Message.BENCHWOKENUP) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();

	}

	public void refWakeUpContestants() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.REFWAKEUPCONTEST);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);

		if (inMessage.getMsgType() != Message.CONTESTWOKENUP) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();

	}

	public void refWakeUpCoachs() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.REFWAKEUPCOAHESB);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);

		if (inMessage.getMsgType() != Message.COAHESWOKENUPB) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();

	}

}
