/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RefereeSiteServer;

import java.net.SocketTimeoutException;

import Comms.Parameters;
import Comms.Server_Com;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class RefereeSiteMain {

	private static final int portNumb = Parameters.getportRefereeSiteServer();
	public static boolean waitConnection = true;

	public static void main(String[] args) {
		RefereeSite refereeSite;
		RefereeSiteInterface refereeSiteInterface;

		Server_Com scon, sconi;
		RefereeSiteAps proxy;

		BenchStub bench = new BenchStub(Parameters.gethostNameBenchServer(), Parameters.getportBenchServer());
		PlaygroundStub playground = new PlaygroundStub(Parameters.gethostNamePlaygroundServer(),
				Parameters.getportPlaygroundServer());
		LoggerStub logger = new LoggerStub(Parameters.gethostNameLoggerServer(), Parameters.getportLoggerServer());

		scon = new Server_Com(portNumb);
		scon.start();

		refereeSite = new RefereeSite(bench, playground, logger);
		refereeSiteInterface = new RefereeSiteInterface(refereeSite);

		System.out.println("O serviço foi establecido");
		System.out.println("O servidor esta em escuta");

		while (waitConnection) {
			try {
				sconi = scon.accept();
				proxy = new RefereeSiteAps(sconi, refereeSiteInterface);
				proxy.start();
			} catch (SocketTimeoutException e) {
			}
		}
		logger.shutdown();
		scon.end();
		System.out.println("O servidor foi desactivado.");
	}
}
