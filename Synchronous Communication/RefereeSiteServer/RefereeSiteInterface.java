/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RefereeSiteServer;

import Comms.Message;
import Comms.MessageException;

/**
 *
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class RefereeSiteInterface {

	RefereeSite refereeSite;

	public RefereeSiteInterface(RefereeSite refereeSite) {
		this.refereeSite = refereeSite;
	}

	public Message processAndReply(Message inMessage) throws MessageException {

		Message outMessage = null;

		// Message processed
		switch (inMessage.getMsgType()) {
		case Message.ANGAME:
			boolean flag = refereeSite.announceNewGame();
			outMessage = new Message(Message.GAMESTARTED, flag);
			break;

		case Message.MATCHWINNER:
			refereeSite.declareMatchWinner();
			outMessage = new Message(Message.MATCHWINNERDECL);
			break;

		case Message.ASSERTTRIAL:
			refereeSite.assertTrialDecision();
			outMessage = new Message(Message.TRIALASSERTED);
			break;

		case Message.SHUT: // shutdown do servidor
			RefereeSiteMain.waitConnection = false;
			(((RefereeSiteAps) (Thread.currentThread())).getScon()).setTimeout(10);
			outMessage = new Message(Message.ACK); // gerar confirmação
			break;
		}
		return outMessage;
	}

}
