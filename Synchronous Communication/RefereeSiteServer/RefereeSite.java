/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RefereeSiteServer;

import Comms.Semaphore;
import Comms.Parameters;
import Comms.RefereeState;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class RefereeSite {

	private Semaphore mutex; // Semáforo para acesso a regiões críticas
	private LoggerStub logger;
	int[] gamesScoreBoard;
	int[] matchesScoreBoard;
	private BenchStub bench;
	private PlaygroundStub playground;
	private int trialsPlayed;
	private int gamesPlayed;

	public RefereeSite(BenchStub bench, PlaygroundStub playground, LoggerStub logger) {
		super();
		
		this.logger = logger;
		this.bench = bench;
		this.playground = playground;
		mutex = new Semaphore();
		mutex.up();
		gamesScoreBoard = new int[Parameters.MAX_TEAMS];
		matchesScoreBoard = new int[Parameters.MAX_TEAMS];
		trialsPlayed = 1;
		gamesPlayed = 0;
	}

	public boolean announceNewGame() {
		mutex.down();
		logger.writeTrialUpdate(trialsPlayed, gamesScoreBoard[1] - gamesScoreBoard[0], gamesPlayed);
		if (!(gamesScoreBoard[0] == Parameters.KNOCKOUT || gamesScoreBoard[1] == Parameters.KNOCKOUT || trialsPlayed == Parameters.MAX_TRIALS)) {
			if (trialsPlayed == 0) {
				logger.setrState(RefereeState.START_OF_A_GAME);
			}
			bench.refWakeUpBench();
		}
		mutex.up();
		gamesScoreBoard = playground.getScoreBoard();
		if (gamesScoreBoard[0] == Parameters.KNOCKOUT || gamesScoreBoard[1] == Parameters.KNOCKOUT || trialsPlayed == Parameters.MAX_TRIALS) {
			if (gamesScoreBoard[0] == Parameters.KNOCKOUT) {
				matchesScoreBoard[0]++;
			}
			if (gamesScoreBoard[1] == Parameters.KNOCKOUT) {
				matchesScoreBoard[1]++;
			}
			if (gamesPlayed < Parameters.MAX_GAMES - 1) {
				logger.writeNewGame(gamesPlayed, trialsPlayed);
				logger.setrState(RefereeState.START_OF_A_GAME);
			}
			playground.clearscoreBoard();
			trialsClear();
			return false;
		}
		return true;
	}

	public void declareMatchWinner() {
		mutex.down();
		gamesPlayed++;
		if (gamesPlayed == Parameters.MAX_GAMES) {
			logger.setrState(RefereeState.END_OF_THE_MATCH);
			logger.writeMatchWinner(matchesScoreBoard);
			bench.endGame();
			bench.refWakeUpBench();
			bench.refWakeUpCoachs();
			bench.refWakeUpContestants();
		}
		mutex.up();
	}

	public void assertTrialDecision() {
		mutex.down();
		trialsPlayed++;
		mutex.up();
	}

	private void trialsClear() {
		gamesScoreBoard[0] = 0;
		gamesScoreBoard[1] = 0;
		trialsPlayed = 0;
	}
}
