/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BenchServer;

import Comms.Parameters;
import Comms.Semaphore;
import Comms.ContestantState;
import Comms.CoachState;
import Comms.RefereeState;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */

public class Bench {

	private Semaphore mutex; // Semáforo para acesso a regiões criticas
	private Semaphore[][] contestantsOnBench; // Semáforos de bloqueio das
	// treads referentes aos
	// Contestants
	private Semaphore[] coachsOnBench; // Semáforos de bloqueio das treads
	// referentes aos Coaches
	private Semaphore refOnBench; // Semáforo de bloqueio das treads referentes
	// ao Referee
	private LoggerStub logger;
	private PlaygroundStub playground;
	private BenchAps[][] team;
	private boolean finaldoJogo;
	private boolean[][] playersInTrial;
	private static int numContestantSeated;
	private int gamesPlayed;
	private int playerNum[];

	public Bench(PlaygroundStub playground, LoggerStub logger) {
		super();
		this.playground = playground;
		this.logger = logger;
		
		team = new BenchAps[Parameters.MAX_TEAMS][Parameters.MAX_CONTESTANTS];
		contestantsOnBench = new Semaphore[Parameters.MAX_TEAMS][Parameters.MAX_CONTESTANTS];
		coachsOnBench = new Semaphore[Parameters.MAX_COACHES];
		refOnBench = new Semaphore();
		mutex = new Semaphore();
		mutex.up();

		playersInTrial = new boolean[Parameters.MAX_TEAMS][Parameters.MAX_CONTESTANTS];
		finaldoJogo = false;

		numContestantSeated = 0;
		gamesPlayed = 0;
		playerNum = new int[2];

		for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
			coachsOnBench[team] = new Semaphore();
			for (int ct = 0; ct < Parameters.MAX_CONTESTANTS; ct++) {
				contestantsOnBench[team][ct] = new Semaphore();
			}
		}
	}

	// Funções dos Contestants
	public boolean seatDown(int teamId, int cntId) {
		contestantsOnBench[teamId][cntId].down();
		mutex.down();

		if (!finaldoJogo) {
			team[teamId][cntId] = (BenchAps) Thread.currentThread();
			numContestantSeated++;
			
			if (numContestantSeated == (Parameters.MAX_CONTESTANTS * Parameters.MAX_TEAMS)) {
				coachsOnBench[0].up();
				coachsOnBench[1].up();
			}
			logger.updateStrength(teamId, cntId, this.team[teamId][cntId].getStrength());
			logger.setctState(ContestantState.SEAT_AT_BENCH, teamId, cntId);
		}
		mutex.up();
		contestantsOnBench[teamId][cntId].down();
		numContestantSeated--;
		if (finaldoJogo) {

			return true;
		}
		return false;
	}

	// Seleciona os jogadores que jogam em função dos semaforos marcados em
	// reviewNotes
	public void followCoachAdvice(int teamId, int cntId) {
		BenchAps ct = (BenchAps) Thread.currentThread();
		mutex.down();
		playersInTrial[teamId][cntId] = true;
		team[teamId][cntId].setContestantSelected(true);
		ct.setContestantSelected(true);
		ct.setStrength(team[teamId][cntId].getStrength());
		ct.setPlaygroundId(playerNum[teamId]);
		
		playerNum[teamId]++;
		if (playerNum[0] == Parameters.MAX_CT_ON_ROPE && playerNum[1] == Parameters.MAX_CT_ON_ROPE) {
			refOnBench.up();
			playerNum[0] = 0;
			playerNum[1] = 0;
		}
		mutex.up();
	}

	// Funções dos Coaches
	public boolean reviewNotes(int teamId) {
		coachsOnBench[teamId].down();
		if (finaldoJogo) {
			return true;
		}
		
		mutex.down();
		if (gamesPlayed != 0) {
			for (int player = 0; player < Parameters.MAX_CONTESTANTS; player++) {
				if (playersInTrial[teamId][player]) {
					this.team[teamId][player].loseStrength();
					logger.updateStrength(teamId, player, this.team[teamId][player].getStrength());
					playersInTrial[teamId][player] = false;
				} else {
					this.team[teamId][player].gainStrength();
					logger.updateStrength(teamId, player, this.team[teamId][player].getStrength());
					playersInTrial[teamId][player] = false;
				}
			}
		}
		logger.setcState(CoachState.WAIT_FOR_REFEREE_COMMAND, teamId);
		mutex.up();
		return false;
	}

	public void callContestants(int teamId, String strategy) {
		mutex.down();
		int[][] choosenPlayer = new int[Parameters.MAX_TEAMS][Parameters.MAX_CT_ON_ROPE];
		// instanciação de array para o metodo contains procurar o contestant
		// com id 0

		for (int i = 0; i < Parameters.MAX_TEAMS; i++) {
			for (int j = 0; j < Parameters.MAX_CT_ON_ROPE; j++) {
				choosenPlayer[i][j] = -1;
			}
		}
		for (int i = 1; i <= 3; i++) {
			if (strategy.equalsIgnoreCase("strongest")) {
				choosenPlayer[teamId][i - 1] = getStrongestCt(teamId, choosenPlayer, i);
			}
		}
		mutex.up();
	}

	private int getStrongestCt(int idTeam, int[][] choosenPlayer, int atempt) {
		int strongest = 0, playerId = 0;
		for (int ct = 1; ct <= Parameters.MAX_CONTESTANTS; ct++) {
			int player = team[idTeam][ct - 1].getContestantId();
			if (team[idTeam][ct - 1].getStrength() > strongest) {
				if (!contains(choosenPlayer, player, Parameters.MAX_TEAMS, Parameters.MAX_CT_ON_ROPE)) {
					strongest = team[idTeam][ct - 1].getStrength();
					choosenPlayer[idTeam][atempt - 1] = team[idTeam][ct - 1].getContestantId();
					playerId = ct - 1;
				}
			}
		}
		contestantsOnBench[idTeam][playerId].up();
		return choosenPlayer[idTeam][atempt - 1];
	}

	private boolean contains(int[][] array, int val, int xSizem, int ySize) {
		for (int i = 0; i < xSizem; i++) {
			for (int j = 0; j < ySize; j++) {
				if (array[i][j] == val) {
					return true;
				}
			}
		}
		return false;
	}

	// Funções do Referee
	public void callTrial() {
		refOnBench.down();
		mutex.down();
		logger.setrState(RefereeState.TEAMS_READY);
		playground.wakeUpCoachesPlayground();
		mutex.up();
	}

	public void assertTrialDecision() {
		mutex.down();
		gamesPlayed++;
		mutex.up();
	}

	public void declareGameWinner() {
		mutex.down();
		int[] gamesScoreBoard = playground.getScoreBoard();
		int matchPlayed = playground.getMatchesNum();
		if ((gamesScoreBoard[0] == Parameters.KNOCKOUT || gamesScoreBoard[1] == Parameters.KNOCKOUT)
				&& matchPlayed < Parameters.MAX_GAMES - 1) {
			logger.setrState(RefereeState.END_OF_A_GAME);
			logger.writeGameWinner(gamesScoreBoard);
		}
		mutex.up();
	}

	public void refWakeUpBench() {
		if (gamesPlayed == 0) {
			for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
				for (int ct = 0; ct < Parameters.MAX_CONTESTANTS; ct++) {
					contestantsOnBench[team][ct].up();
				}
			}
		} else {
			for (int ctTeam = 0; ctTeam < Parameters.MAX_TEAMS; ctTeam++) {
				for (int ct = 0; ct < Parameters.MAX_CONTESTANTS; ct++) {
					if (team[ctTeam][ct].isContestantSelected()) {
						contestantsOnBench[ctTeam][ct].up();
						team[ctTeam][ct].setContestantSelected(false);
						team[ctTeam][ct].setPlaygroundId(-1);
					}
				}
			}
		}
	}

	public void refWakeUpContestants() {
		for (int ctTeam = 0; ctTeam < Parameters.MAX_TEAMS; ctTeam++) {
			for (int ct = 0; ct < Parameters.MAX_CONTESTANTS; ct++) {
				contestantsOnBench[ctTeam][ct].up();
			}
		}
	}

	public void refWakeUpCoachs() {
		for (int team = 0; team < Parameters.MAX_TEAMS; team++) {
			coachsOnBench[team].up();
		}
	}

	public void endGame() {
		finaldoJogo = true;
	}

}
