/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BenchServer;

import Comms.Server_Com;

import java.net.SocketTimeoutException;

import Comms.Parameters;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class MainBench {

	private static final int portNumb = Parameters.getportBenchServer();
	public static boolean waitConnection = true;

	public static void main(String[] args) {
		Bench bench;
		BenchInterface benchInterface;

		Server_Com scon, sconi;
		BenchAps contestantproxy;

		PlaygroundStub playground = new PlaygroundStub(Parameters.gethostNamePlaygroundServer(),
				Parameters.getportPlaygroundServer());
		LoggerStub logger = new LoggerStub(Parameters.gethostNameLoggerServer(), Parameters.getportLoggerServer());

		scon = new Server_Com(portNumb);
		scon.start();

		bench = new Bench(playground, logger);
		benchInterface = new BenchInterface(bench);

		System.out.println("O serviço foi establecido");
		System.out.println("O servidor esta em escuta");

		while (waitConnection) {
			try {
				sconi = scon.accept();
				contestantproxy = new BenchAps(sconi, benchInterface);
				contestantproxy.start();
			} catch (SocketTimeoutException e) {
			}
		}
		logger.shutdown();
		scon.end();
		System.out.println("O servidor foi desactivado.");
	}
}
