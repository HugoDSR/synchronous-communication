/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BenchServer;

import Comms.Server_Com;
import Comms.Message;
import Comms.MessageException;
import Comms.CoachState;
import Comms.ContestantState;
import Comms.RefereeState;
import genclass.GenericIO;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class BenchAps extends Thread {

	private static int nProxy = 0;
	private Server_Com sconi;
	private BenchInterface benchInterface;

	private CoachState coachState;
	private int coachTeam;
	private String strategy;

	private ContestantState contestantState;
	private int contestantId, contestantTeam, strength, playgroundId;
	private boolean contestantSelected;

	private RefereeState refereeState;

	public BenchAps(Server_Com sconi, BenchInterface benchInterface) {
		super("Proxy_" + BenchAps.getProxyId());

		this.sconi = sconi;
		this.benchInterface = benchInterface;
	}

	@Override
	public void run() {
		Message inMessage = null, // mensagem de entrada
				outMessage = null; // mensagem de saída
		String inString, outString;

		inString = (String) sconi.readObject(); // ler pedido do cliente
		inMessage = new Message(inString);
		try {
			outMessage = benchInterface.processAndReply(inMessage); // processá-lo
		} catch (MessageException ex) {
			GenericIO.writelnString("Thread " + getName() + ": " + ex.getMessage() + "!");
			GenericIO.writelnString(ex.getMessage().toString());
			System.exit(1);
		}

		outString = outMessage.toXMLString();
		sconi.writeObject(outString); // enviar resposta ao cliente
		sconi.close(); // fechar canal de comunicação
	}

	private static int getProxyId() {
		Class<?> cl = null; // representação do tipo de dados ClientProxy na
							// máquina
		// virtual de Java
		int proxyId; // identificador da instanciação

		try {
			cl = Class.forName("BenchServer.BenchAps");
		} catch (ClassNotFoundException e) {
			GenericIO.writelnString("O tipo de dados ClientProxy não foi encontrado!");
			e.printStackTrace();
			System.exit(1);
		}

		synchronized (cl) {
			proxyId = nProxy;
			nProxy += 1;
		}

		return proxyId;
	}

	public Server_Com getScon() {
		return sconi;
	}

	public CoachState getCoachState() {
		return coachState;
	}

	public void setCoachState(CoachState coachState) {
		this.coachState = coachState;
	}

	public int getCoachTeam() {
		return coachTeam;
	}

	public void setCoachTeam(int coachTeam) {
		this.coachTeam = coachTeam;
	}

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	public ContestantState getContestantState() {
		return contestantState;
	}

	public void setContestantState(ContestantState contestantState) {
		this.contestantState = contestantState;
	}

	public int getContestantId() {
		return contestantId;
	}

	public void setContestantId(int contestantId) {
		this.contestantId = contestantId;
	}

	public int getContestantTeam() {
		return contestantTeam;
	}

	public void setContestantTeam(int contestantTeam) {
		this.contestantTeam = contestantTeam;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public void gainStrength() {
		this.strength++;
	}

	public void loseStrength() {
		this.strength--;
	}

	public int getPlaygroundId() {
		return playgroundId;
	}

	public void setPlaygroundId(int playgroundId) {
		this.playgroundId = playgroundId;
	}

	public boolean isContestantSelected() {
		return contestantSelected;
	}

	public void setContestantSelected(boolean contestantSelected) {
		this.contestantSelected = contestantSelected;
	}

	public RefereeState getRefereeState() {
		return refereeState;
	}

	public void setRefereeState(RefereeState refereeState) {
		this.refereeState = refereeState;
	}

}
