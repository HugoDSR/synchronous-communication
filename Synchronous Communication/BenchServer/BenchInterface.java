/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BenchServer;

import Comms.Message;
import Comms.MessageException;
import Comms.Parameters;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class BenchInterface {

	private Bench bench;
	private volatile int shut;

	public BenchInterface(Bench bench) {
		this.bench = bench;
	}

	public Message processAndReply(Message inMessage) throws MessageException {

		BenchAps proxy;
		Message outMessage = null;

		// message received validation
		switch (inMessage.getMsgType()) {
		case Message.CNTSEAT:
			if (inMessage.getContestantTeam() == -1 || inMessage.getContestantId() == -1) {
				throw new MessageException("Dados de entrada de Contestant para o metodo SeatDown() invalidos!",
						inMessage);
			}
			break;
		case Message.CNTFOLLOW:
			if (inMessage.getContestantTeam() == -1 || inMessage.getContestantId() == -1) {
				throw new MessageException(
						"Dados de entrada de Contestant para o metodo FollowCoachAdvice() invalidos!", inMessage);
			}
			break;
		case Message.COAREVNOTES:
			if (inMessage.getCoachTeam() == -1) {
				throw new MessageException("Dados de entrada de Coach para o metodo ReviewNotes() invalidos!",
						inMessage);
			}
			break;
		case Message.COACALLSCONT:
			if (inMessage.getCoachTeam() == -1 || inMessage.getCoachStrategy() == "") {
				throw new MessageException("Dados de entrada de Coach para o metodo CallContestant() invalidos!",
						inMessage);
			}
			break;
		}

		// Message processed
		switch (inMessage.getMsgType()) {

		case Message.CNTSEAT:
			proxy = (BenchAps) Thread.currentThread();
			proxy.setContestantId(inMessage.getContestantId());
			proxy.setContestantTeam(inMessage.getContestantTeam());
			proxy.setStrength(inMessage.getContestantStrength());
			proxy.setContestantSelected(false);
			boolean flag = bench.seatDown(inMessage.getContestantTeam(), inMessage.getContestantId());
			outMessage = new Message(Message.SEATEDCONT, flag);
			break;

		case Message.CNTFOLLOW:
			proxy = (BenchAps) Thread.currentThread();
			bench.followCoachAdvice(inMessage.getContestantTeam(), inMessage.getContestantId());
			outMessage = new Message(Message.FOLLOWEDADV, proxy.getPlaygroundId(), proxy.getStrength());
			break;

		case Message.COAREVNOTES:
			boolean flag2 = bench.reviewNotes(inMessage.getCoachTeam());
			outMessage = new Message(Message.COANOTESREV, flag2);
			break;
		case Message.COACALLSCONT:
			bench.callContestants(inMessage.getCoachTeam(), inMessage.getCoachStrategy());
			outMessage = new Message(Message.CONTCALLED);
			break;

		case Message.CALLTRIAL:
			bench.callTrial();
			outMessage = new Message(Message.TRIALCALLED);
			break;

		case Message.ASSERTTRIAL:
			bench.assertTrialDecision();
			outMessage = new Message(Message.TRIALASSERTED);
			break;

		case Message.GAMEWINNER:
			bench.declareGameWinner();
			outMessage = new Message(Message.GAMEWINNERDECL);
			break;

		case Message.REFWAKEUPBENCH:
			bench.refWakeUpBench();
			outMessage = new Message(Message.BENCHWOKENUP);
			break;

		case Message.REFWAKEUPCONTEST:
			bench.refWakeUpContestants();
			outMessage = new Message(Message.CONTESTWOKENUP);
			break;
		case Message.REFWAKEUPCOAHESB:
			bench.refWakeUpCoachs();
			outMessage = new Message(Message.COAHESWOKENUPB);
			break;
		case Message.REFENDSGAME:
			bench.endGame();
			outMessage = new Message(Message.GAMEENDED);
			break;
		case Message.SHUT: // shutdown do servidor
			shut++;
			if (shut == Parameters.SHUTDOWN) {
				MainBench.waitConnection = false;
				(((BenchAps) (Thread.currentThread())).getScon()).setTimeout(10);
			}
			outMessage = new Message(Message.ACK); // gerar confirmação
			break;
		}
		return outMessage;
	}

}
