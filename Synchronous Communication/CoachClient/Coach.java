/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoachClient;

/**
 *
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class Coach extends Thread {

	/**
	 * Coach Id
	 *
	 * @serialField coachId
	 */
	private int coachId;

	/**
	 * Coach Strategy
	 *
	 * @serialField strategy
	 */
	private String strategy;

	/**
	 * Stub of shared regions
	 *
	 * @serialField bench
	 * @serialField playground
	 */
	private BenchStub benchStub;
	private PlaygroundStub playgroundStub;

	/**
	 * Instantiation of a Coach
	 *
	 * @param coachId
	 *            coach identification
	 * @param strategy
	 *            coach strategy
	 * @param benchStub
	 *            stub to bench
	 * @param playgroundStub
	 *            stub to playground
	 */

	public Coach(int coachId, String strategy, BenchStub benchStub, PlaygroundStub playgroundStub) {
		super("Coach_" + coachId);

		this.coachId = coachId;
		this.strategy = strategy;
		this.benchStub = benchStub;
		this.playgroundStub = playgroundStub;
	}

	/**
	 * Life cycle of referee thread
	 */
	@Override
	public void run() {
		System.out.println("O Coach "+coachId+" começou");
		while (!benchStub.reviewNotes(coachId)) {
			benchStub.callContestants(coachId, strategy);
			playgroundStub.callContestants(coachId);
			playgroundStub.informReferee(coachId);
		}
		System.out.println("O Coach " + coachId + " acabou");
	}
}
