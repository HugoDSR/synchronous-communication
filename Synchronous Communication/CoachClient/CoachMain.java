/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoachClient;

import Comms.Parameters;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class CoachMain {

	public static void main(String[] args) {

		BenchStub bench = new BenchStub(Parameters.gethostNameBenchServer(), Parameters.getportBenchServer());
		PlaygroundStub playground = new PlaygroundStub(Parameters.gethostNamePlaygroundServer(),
				Parameters.getportPlaygroundServer());

		/* Thread Creation */
		Coach[] coach = new Coach[2];
		for (int i = 0; i < 2; i++) {
			coach[i] = new Coach(i, "strongest", bench, playground);
		}

		/* Simulation kickoff */
		for (int i = 0; i < 2; i++) {
			coach[i].start();
		}

		/* Waits simulation to end */
		try {
			for (int i = 0; i < 2; i++) {
				coach[i].join();
			}
		} catch (InterruptedException e) {
		}
		bench.shutdown();
		playground.shutdown();
	}

}
