package RefereeClient;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class Referee extends Thread {

	/**
	 * Referee Id
	 *
	 * @serialField refereeId
	 */
	private int refereeId;

	/**
	 * Stub of shared regions
	 *
	 * @serialField bench
	 * @serialField playground
	 * @serialField refereeSite
	 */
	private BenchStub benchStub;
	private PlaygroundStub playgroundStub;
	private RefereeSiteStub refereeSiteStub;

	/**
	 * Instantiation of a Referee
	 *
	 * @param refereeId
	 *            referee identification
	 * @param benchStub
	 *            stub to bench
	 * @param playgroundStub
	 *            stub to playground
	 * @param refereeSiteStub
	 *            stub to refereeSite
	 */
	public Referee(int refereeId, BenchStub benchStub, PlaygroundStub playgroundStub, RefereeSiteStub refereeSiteStub) {
		super("Referee_" + refereeId);

		this.refereeId = refereeId;
		this.benchStub = benchStub;
		this.playgroundStub = playgroundStub;
		this.refereeSiteStub = refereeSiteStub;
	}

	/**
	 * Life cycle of referee thread
	 */
	@Override
	public void run() {
		System.out.println("O Referee Começa");
		for (int g = 0; g < 3; g++) {
			while (refereeSiteStub.announceNewGame()) {
				benchStub.callTrial();
				playgroundStub.startTrial();
				refereeSiteStub.assertTrialDecision();
				benchStub.assertTrialDecision();
				playgroundStub.assertTrialDecision();
				benchStub.declareGameWinner();
			}
			refereeSiteStub.declareMatchWinner();
		}
		System.out.println("O Referee Termina");
	}

}
