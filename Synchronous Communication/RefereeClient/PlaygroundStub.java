/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RefereeClient;

import Comms.*;
import genclass.GenericIO;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class PlaygroundStub {

	/**
	 * Name of computational sister where server is located
	 *
	 * @SerialField serverHostName
	 */
	private String serverHostName;

	/**
	 * Number of server listening port
	 *
	 * @SerialField serverPortNumber
	 */
	private int serverPortNumber;

	/**
	 * PlaygroundStub Instantiation
	 *
	 * @param hostname
	 *            name of the system where server is located
	 * @param port
	 *            number of server listening port
	 *
	 */
	public PlaygroundStub(String hostname, int port) {
		serverHostName = hostname;
		serverPortNumber = port;
	}

	public void startTrial() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.STARTTRIAL);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);
		if ((inMessage.getMsgType() != Message.TRIALSTARTED)) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
	}

	public void assertTrialDecision() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.ASSERTTRIAL);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);
		if ((inMessage.getMsgType() != Message.TRIALASSERTED)) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
	}

	public void shutdown() {
		Client_Com con = new Client_Com(serverHostName, serverPortNumber);
		Message inMessage, outMessage;
		String inString, outString;
		while (!con.open()) // aguarda ligação
		{
			try {
				Thread.currentThread().sleep((long) (500));
			} catch (InterruptedException e) {
			}
		}
		outMessage = new Message(Message.SHUT);
		outString = outMessage.toXMLString();
		con.writeObject(outString);
		inString = (String) con.readObject();
		inMessage = new Message(inString);
		if ((inMessage.getMsgType() != Message.ACK)) {
			GenericIO.writelnString("Thread " + Thread.currentThread().getName() + ": Tipo inválido!");
			GenericIO.writelnString(inMessage.toString());
			System.exit(1);
		}
		con.close();
	}

}
