package RefereeClient;

import Comms.Parameters;

/**
 * @author Hugo Rocha nmec 68511
 * @author Miguel Antunes nmec 68484
 */
public class RefereeMain {

	public static void main(String[] args) {

		BenchStub benchStub;
		PlaygroundStub playgroundStub;
		RefereeSiteStub refereeSiteStub;

		benchStub = new BenchStub(Parameters.gethostNameBenchServer(), Parameters.getportBenchServer());
		playgroundStub = new PlaygroundStub(Parameters.gethostNamePlaygroundServer(),
				Parameters.getportPlaygroundServer());
		refereeSiteStub = new RefereeSiteStub(Parameters.gethostNameRefereeSiteServer(),
				Parameters.getportRefereeSiteServer());

		/* Thread Creation */
		Referee ref = new Referee(1, benchStub, playgroundStub, refereeSiteStub);

		/* Simulation kickoff */
		ref.start();

		/* Waits simulation to end */
		try {
			ref.join();
		} catch (InterruptedException e) {
		}
		
		benchStub.shutdown();
		playgroundStub.shutdown();
		refereeSiteStub.shutdown();
	}

}
